#include <stdio.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>
#include <vector>
#include <dirent.h> // deals with path and files inside

#ifndef C_POINTS
#define C_POINTS
   #include "Points.cpp"
#endif
#ifndef C_MESH
#define C_MESH
   #include "Mesh.cpp"
#endif
#ifndef C_SVG
#define C_SVG
   #include "SVG.cpp"
#endif
#ifndef C_COLOUR
#define C_COLOUR
   #include "Colour.cpp"
#endif

#include "OpenCVDemo.cpp"


using namespace cv;

int main(int argc, char** argv )
{
   if ( argc < 2 ){
      printf("usage: quality <Test_Path>\n");
      return -1;
   }
   Colour cl;
   Mat img, lab;
   Points points;
   Mesh ms;
   std::string path = argv[1];

   DIR *dir;
   struct dirent *ent;
   if ((dir = opendir (argv[1])) != NULL) {
      while ((ent = readdir (dir)) != NULL) {
         std::string nm = ent->d_name;
         int endfName = nm.rfind('.');
         std::string fName = nm.substr(0, endfName);
         std::string ext = nm.substr(endfName+1, -1);
         
         if(ext == "jpg" || ext == "JPG" || ext == "png" || ext == "PNG" || ext == "jpeg" || ext == "JPEG"){
            img = imread( path+"/"+ent->d_name, CV_LOAD_IMAGE_COLOR );
            cl.toLAB(img, lab);
            
            Mat tri = Mat::zeros(img.size(), img.type());
            Mat msh = Mat::zeros(img.size(), img.type());
            // Print Points
            points.print(points.USE_GOODFEATURE, img, lab);
            imwrite(path+"/samples/"+fName+"_pts."+ext, lab);
            // Print mesh
            std::vector<Point2f> ptsList;
            points.getPoints(Points::GOODFEATURE, lab, ptsList);

            // Here I beack encapsulation and do some of the work that Mesh does in private, only so I can print only the mesh
            Size size = img.size();
            Rect rect(0, 0, size.width, size.height);
            Subdiv2D sdiv(rect);
            sdiv.insert(ptsList);
            msh = img.clone();
            draw_delaunay(msh, sdiv, Scalar(0, 255, 120));

            imwrite(path+"/samples/"+fName+"_msh."+ext, msh);
            // Print triangulation
            ms.triangulate(Mesh::DELAUNAY, ptsList, img);
            ms.print(tri);

            imwrite(path+"/samples/"+fName+"_tri."+ext, tri);

            std::cout<<fName<<"\n";

         }
      }
      closedir (dir);
   } else {
      printf("Error: Could not open the provided path.\n");
      return -1;
   }

   
   return 0;
}













/*


*/