#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <vector>
#include <string>

#ifndef C_SHAPE
#define C_SHAPE
   #include "Shape.cpp"
#endif

using namespace cv;

class Circle: public Shape{
   Point center;
   int radius;
public:
   Point getCenter();
   void setCenter(Point c);
   int getRadius();
   void setRadius(int r);
   void setColour(Mat &img, int nPoints, int reductionFactor);
   void draw(Mat &img);

   Circle();
   Circle(Point c, int r);
};

Point Circle::getCenter(){
   return center;
}
void Circle::setCenter(Point c){
   center = c;
}
int Circle::getRadius(){
   return radius;
}
void Circle::setRadius(int r){
   radius = r;
}

void Circle::setColour(Mat &img, int nPoints = 10, int reductionFactor = 5){
   std::vector<Point> samples;
   int minCol = center.x-radius/reductionFactor;
   int maxCol = center.x+radius/reductionFactor;
   int minRow = center.y-radius/reductionFactor;
   int maxRow = center.y+radius/reductionFactor;
   // Ensuring that I keep in the img's boundries (avoid segfault)
   maxCol = (maxCol >= img.cols) ? img.cols-1 : maxCol;
   maxRow = (maxRow >= img.rows) ? img.rows-1 : maxRow;
   minCol = (minCol < 0) ? 0 : minCol;
   minRow = (minRow < 0) ? 0 : minRow;
   minCol = (minCol>maxCol) ? maxCol : minCol;
   minRow = (minRow>maxRow) ? maxRow : minRow;
   for(int i = 0; i < nPoints; i++){
      samples.push_back(Point(random(minCol, maxCol), random(minRow, maxRow)));
   }
   Shape::setColour(img, samples);
}

void Circle::draw(Mat &img){
   circle(img, center, radius, Shape::getColour(), CV_FILLED, CV_AA, 0 );
}

Circle::Circle(){
   setCenter(Point(0,0));
   setRadius(0);
}
Circle::Circle(Point c, int r){
   setCenter(c);
   setRadius(r);
}

