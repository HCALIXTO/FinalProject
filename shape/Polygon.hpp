#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <vector>
#include <string>

#ifndef C_SHAPE
#define C_SHAPE
   #include "Shape.cpp"
#endif

using namespace cv;

class Polygon: public Shape{
   int id;
   std::vector<Point> points;
   Point center;
   Point calculateCenter();
   bool pointInside(Point point);
   void setCenter();
public:
   Point getCenter();
   std::vector<Point> getPoints();
   void setId(int idIn);
   int getId();
   void setPoints(std::vector<Point> pts);
   void setPoints(Polygon poly);
   void setPoints(Vec6f triangle);
   void addPoint(Point p);
   void removePoint(Point p);
   int numberOfPoints();
   void setColour(Mat &img, int nPoints, int reductionFactor);
   using Shape::setColour; // Need to do it so this setColour dont overide the others defined by Shape
   void draw(Mat &img);

   Polygon();
   Polygon(std::vector<Point> pts);
   Polygon(std::vector<Point> pts, Mat &img);
   Polygon(Vec6f triangle);
   Polygon(Vec6f triangle, Mat &img);
};
// Private functions
// Calculates the centroid of a Polygon according to https://en.wikipedia.org/wiki/Centroid#Centroid_of_a_polygon
// source: https://stackoverflow.com/questions/2792443/finding-the-centroid-of-a-polygon
// It only works if the points are in the order they apear in the polygon IMPORTANT
Point Polygon::calculateCenter(){
   int nPoints = numberOfPoints();
   Point centroid = Point(0, 0);
   if(nPoints > 0){
      double signedArea = 0.0;
      int x0 = 0.0; // Current vertex X
      int y0 = 0.0; // Current vertex Y
      int x1 = 0.0; // Next vertex X
      int y1 = 0.0; // Next vertex Y
      double a = 0.0;  // Partial signed area

      // For all points except last
      int i=0;
      for (; i<nPoints-1; ++i){
         x0 = points.at(i).x;
         y0 = points.at(i).y;
         x1 = points.at(i+1).x;
         y1 = points.at(i+1).y;
         a = x0*y1 - x1*y0;
         signedArea += a;
         centroid.x += (x0 + x1)*a;
         centroid.y += (y0 + y1)*a;
      }

      // Do last vertex separately to avoid performing an expensive
      // modulus operation in each iteration.
      x0 = points.at(i).x;
      y0 = points.at(i).y;
      x1 = points.at(0).x;
      y1 = points.at(0).y;
      a = x0*y1 - x1*y0;
      signedArea += a;
      centroid.x += (x0 + x1)*a;
      centroid.y += (y0 + y1)*a;

      signedArea *= 0.5;
      centroid.x /= (6.0*signedArea);
      centroid.y /= (6.0*signedArea);
   }
   return Point(centroid.y, centroid.x);// I have to do it to ensure col row order
}

// Check if a point is inside a polygon, it assumes if it is over the line it is out
// it uses the algorithm ray-casting to the right
// source: https://stackoverflow.com/questions/11716268/point-in-polygon-algorithm
bool Polygon::pointInside(Point point) {
  int i, j, nvert = points.size();
  bool c = false;

  for(i = 0, j = nvert - 1; i < nvert; j = i++) {
    if( ( (points[i].y >= point.y ) != (points[j].y >= point.y) ) &&
        (point.x <= (points[j].x - points[i].x) * (point.y - points[i].y) / (points[j].y - points[i].y) + points[i].x)
      )
      c = !c;
  }

  return c;
}

void Polygon::setCenter(){
   center = calculateCenter();
}

// Public functions

Point Polygon::getCenter(){
   if(center.x == 0 && center.y == 0){
      setCenter();
   }
   std::cout<<"Center (x, y)"<<center.x<<" | "<<center.y<<"\n";
   return Point(center.x, center.y);
}

std::vector<Point> Polygon::getPoints(){
   return points;
}

void Polygon::setId(int idIn){
   id = idIn;
}

int Polygon::getId(){
   return id;
}

void Polygon::setPoints(std::vector<Point> pts){
   points = pts;
   center = Point(0, 0);// To force the center to be recalculated if needed
}

void Polygon::setPoints(Polygon poly){
   std::vector<Point> polyPts;
   std::vector<Point> myPts; // create a empty set of points
   points = myPts; // Clear the points
   polyPts = poly.getPoints();
   for(int i = 0;i<polyPts.size();i++){
      Point pt = polyPts[i];
      addPoint(Point(pt.x,pt.y));
   }
}

void Polygon::setPoints(Vec6f triangle){
   std::vector<Point> pts;
   pts.push_back(Point(triangle[0],triangle[1]));
   pts.push_back(Point(triangle[2],triangle[3]));
   pts.push_back(Point(triangle[4],triangle[5]));
   setPoints(pts);
}

void Polygon::addPoint(Point p){
   points.push_back(p);
   center = Point(0, 0);// To force the center to be recalculated if needed
}

void Polygon::removePoint(Point p){
   points.erase(std::remove(points.begin(), points.end(), p), points.end());
   center = Point(0, 0);// To force the center to be recalculated if needed
}

int Polygon::numberOfPoints(){
   return points.size();
}

void Polygon::setColour(Mat &img, int nPoints = 10, int reductionFactor = 5){
   if(numberOfPoints() > 0){
      std::vector<Point> samples;
      int trys = 0, nSamples = 0;
      int limit = nPoints*3; // Limit for the number of tries
      Point pt1 = points.at(0);
      int minCol = pt1.x, maxCol = pt1.x, minRow = pt1.y, maxRow = pt1.y;
      
      for(std::vector<Point>::iterator it = points.begin(); it != points.end(); it++){
         minCol = (minCol > it->x) ? it->x : minCol;
         maxCol = (maxCol < it->x) ? it->x : maxCol;
         minRow = (minRow > it->y) ? it->y : minRow;
         maxRow = (maxRow < it->y) ? it->y : maxRow;  
      }
      // Ensuring that I keep in the img's boundries (avoid segfault)
      maxCol = (maxCol >= img.cols) ? img.cols-1 : maxCol;
      maxRow = (maxRow >= img.rows) ? img.rows-1 : maxRow;
      minCol = (minCol < 0) ? 0 : minCol;
      minRow = (minRow < 0) ? 0 : minRow;
      minCol = (minCol>maxCol) ? maxCol : minCol;
      minRow = (minRow>maxRow) ? maxRow : minRow;
      
      while(nSamples < nPoints && trys < limit){
         Point pt = Point(random(minCol, maxCol), random(minRow, maxRow));//Generate a random point in the limit
         if(pointInside(pt)){// If the point is inside the polygon it becames sample
            samples.push_back(pt);
            nSamples++;
         }
         trys++;
      }
      // If I can't find a point inside the polygon I just use it's points as sample
      if(samples.size() == 0){
         samples = points; 
      }
      //std::cout<<"Sample: "<<samples<<"\n";
      Shape::setColour(img, samples);
   }
}

void Polygon::draw(Mat &img){
   const Point *ppt[1] = {&points[0]};
   const int nPts[] = {numberOfPoints()};
   fillPoly(img, ppt, nPts, 1, getColour());//Shape::getColour());
}

Polygon::Polygon(){
   center = Point(0, 0);
}

Polygon::Polygon(std::vector<Point> pts){
   setPoints(pts);
}
Polygon::Polygon(std::vector<Point> pts, Mat &img){
   setPoints(pts);
}
Polygon::Polygon(Vec6f triangle){
   setPoints(triangle);
}
Polygon::Polygon(Vec6f triangle, Mat &img){
   setPoints(triangle);
}