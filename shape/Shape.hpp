#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <vector>
#include <string>

#ifndef C_COLOUR
#define C_COLOUR
   #include "Colour.cpp"
#endif

using namespace cv;

class Shape{
   Colour colour;
public:
   int random(int min, int max);   
   Scalar getColour();
   std::string getHexColour();
   Colour getColourObj();
   void setColour(Scalar c);
   void setColour(Colour c);
   void setColour(Mat &img, std::vector<Point> samples);
   virtual void setColour(Mat &img, int nPoints, int reductionFactor);
   bool compareColours(Shape s, int threshold);

   Shape();
};

// return a random number in a range
int Shape::random(int min, int max){
   static bool first = true;
   if (first) {  
      srand( time(NULL) ); //seeding for the first time only!
      first = false;
   }
   return min + rand() % (( max + 1 ) - min);
}

Scalar Shape::getColour(){
   return colour.getColour();
}
std::string Shape::getHexColour(){
   return colour.getHexColour();
}
Colour Shape::getColourObj(){
   return colour;
}
void Shape::setColour(Scalar c){
   colour = Colour(c);
}
void Shape::setColour(Colour c){
   colour = c;
}
void Shape::setColour(Mat &img, std::vector<Point> samples){
   Colour c;
   colour = c;
   colour.setColour(img, samples);
}
void Shape::setColour(Mat &img, int nPoints, int reductionFactor){
   std::cout<<"Running the shape version\n";
   setColour(Scalar(0,0,0,0));
}
bool Shape::compareColours(Shape s, int threshold){
   return colour.compare(s.getColourObj(), threshold);
}
Shape::Shape(){
   setColour(Scalar(0,0,0,0));
}
