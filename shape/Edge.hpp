#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <vector>

#ifndef C_POLY
#define C_POLY
   #include "Polygon.hpp"
#endif

class Edge{
   Point p1;
   Point p2;
   int cost;
   
public:
   void setCost(int c);
   int getCost();
   Point getP1();
   Point getP2();
   void print();
   Edge(Vec4f pts);
   Edge(Point pt1, Point pt2);
   //~Edge();
   
};

void Edge::setCost(int c){
   cost = c;
}

int Edge::getCost(){
   return cost;
}

Point Edge::getP1(){
   return p1;
}

Point Edge::getP2(){
   return p2;
}

void Edge::print(){
   std::cout<<"Edge: ("<<p1.x<<" , "<<p1.y<<")---("<<p2.x<<" , "<<p2.y<<")\n";
}

Edge::Edge(Vec4f pts){
   p1 = Point(pts[0],pts[1]);
   p2 = Point(pts[2],pts[3]);
}

Edge::Edge(Point pt1, Point pt2){
   p1 = pt1;
   p2 = pt2;
}