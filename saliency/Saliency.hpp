#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace cv;

class Saliency {
   private:
      void mCanny(const Mat & source, Mat &dst);
   public:
      enum PrintMode {FULL, BY_CHANNEL, USE_CANNY};
      enum SaliencyMethod {CANNY, THRESHOLD};

      Saliency();
      void toLAB(const Mat & source, Mat & destine);
      void print(const Mat & source, PrintMode mode);
      void getSaliency(SaliencyMethod method, const Mat & source, Mat &dst);
      ~Saliency();
};

// Initialize the object defining the base image according to source
Saliency::Saliency(){
   
}

// Class destructor
Saliency::~Saliency(){
   //std::cout<<"\nGoodbye...\n";
}

// Set the base image, get the source and change it's colourspace to LAB
void Saliency::toLAB(const Mat & source, Mat & destine){
   // If the bit depth is not 8-bit unsigned I'll convert to it
   if(source.depth() != CV_8U){
      source.convertTo(source, CV_8U);//, 1.0/65535.0f
   }
   cvtColor(source, destine, CV_BGR2Lab);
}

// Print the base image accordingly to the selected mode
void Saliency::print(const Mat & source, PrintMode mode){
   Mat ch[3];
   Mat salMap;
   Mat labImg;
   toLAB(source, labImg);

   switch(mode){
      case FULL:
         namedWindow("Full LAB Image", WINDOW_NORMAL );
         imshow("Full LAB Image", labImg);
         break;

      case BY_CHANNEL:
         split(labImg, ch);
         namedWindow("L Channel", WINDOW_NORMAL );
         imshow("L Channel", ch[0]);
         namedWindow("A Channel", WINDOW_NORMAL );
         imshow("A Channel", ch[1]);
         namedWindow("B Channel", WINDOW_NORMAL );
         imshow("B Channel", ch[2]);
         break;

      case USE_CANNY:
         getSaliency(CANNY, labImg, salMap);
         namedWindow("CANNY Feature MAP", WINDOW_NORMAL );
         imshow("CANNY Feature MAP", salMap);
         break;

      default:
         std::cout<<"Wrong print mode on Saliency.print().";
   }
}

// Return the saliency map accordingly to the desired method
void Saliency::getSaliency(SaliencyMethod method, const Mat & source, Mat &dst){
   Mat labImg;
   toLAB(source, labImg);

   switch(method){
      case CANNY:
         mCanny(labImg, dst);
         break;

      default:
         std::cout<<"Wrong saliency method on Saliency.getSaliency.";
   }
}

// Returns the map obtained using canny method
void Saliency::mCanny(const Mat & source, Mat &dst){
   Mat blurred;
   // Blur the image to reduce the amount of noise
   GaussianBlur( source, blurred, Size( 5, 5 ), 0, 0 );// The maximum size is 31
   // The smallest value between threshold1 and threshold2 is used for edge linking.
   // The largest value is used to find initial segments of strong edges. 
   // See http://en.wikipedia.org/wiki/Canny_edge_detector
   Canny( blurred, dst, 240, 20, 3 );//Canny( source, dst, 50, 200, 3 );
}