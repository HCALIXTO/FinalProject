#include <stdio.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <dirent.h> // deals with path and files inside

#ifndef C_INTERPRETER
#define C_INTERPRETER
   #include "Interpreter.cpp"
#endif
#ifndef C_MESH
#define C_MESH
   #include "Mesh.cpp"
#endif
#ifndef C_SVG
#define C_SVG
   #include "SVG.cpp"
#endif

extern "C" {
  #include "lsd.h" //a C header, so wrap it in extern "C" 
}

using namespace cv;

int main(int argc, char** argv )
{
   if ( argc < 2 ){
      printf("usage: benchmark <Test_Path>\n");
      return -1;
   }
   Interpreter interp;
   Mat img, imgGray, compressed;

   std::string path = argv[1];

   DIR *dir;
   struct dirent *ent;
   if ((dir = opendir (argv[1])) != NULL) {
      while ((ent = readdir (dir)) != NULL) {
         std::string nm = ent->d_name;
         int endfName = nm.rfind('.');
         std::string fName = nm.substr(0, endfName);
         std::string ext = nm.substr(endfName+1, -1);
         
         std::cout<<fName<<"\n";
         if(ext == "pgm" || ext == "jpg" || ext == "JPG" || ext == "png" || ext == "PNG" || ext == "jpeg" || ext == "JPEG"){
            img = imread( path+"/"+ent->d_name, CV_LOAD_IMAGE_COLOR );
            imgGray = imread( path+"/"+ent->d_name, IMREAD_GRAYSCALE );
            Ptr<LineSegmentDetector> ls = createLineSegmentDetector(LSD_REFINE_NONE);// LSD_REFINE_NONE or LSD_REFINE_STD
            std::vector<Vec4f> lines_std;
            // Detect the lines
            ls->detect(imgGray, lines_std);
            Mesh m = interp.addEdgesToMesh(img, lines_std);
            std::cout<<"Got to SVG...\n";
            SVG svg(img);
            svg.add(m.getPolygons());
            svg.save(fName+"_svg", path+"/");
            /*
            // Show found lines
            Mat drawnLines(imgGray);
            ls->drawSegments(drawnLines, lines_std);
            namedWindow("LSD Test", WINDOW_NORMAL );
            imshow("LSD Test", drawnLines);
            waitKey();
            */
            
         }
      }
      closedir (dir);
   } else {
      printf("Error: Could not open the provided path.\n");
      return -1;
   }

   
   return 0;
}
