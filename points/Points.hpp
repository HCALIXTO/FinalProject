#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace cv;

class Points {
   private:
      void mGoodFeatures(Mat &src, std::vector<Point2f> &corners);
   public:
      enum PrintMode {USE_GOODFEATURE};
      enum PointsMethod {GOODFEATURE};

      Points();
      void print(PrintMode mode, Mat &src, Mat &dest);
      void getPoints(PointsMethod method, Mat &src, std::vector<Point2f> &corners);
      ~Points();
};

// Class constructor
Points::Points(){
}

// Class destructor
Points::~Points(){
   //std::cout<<"\nGoodbye...\n";
}

// Print the base image accordingly to the selected mode
void Points::print(PrintMode mode, Mat &src, Mat &dest){
   std::vector<Point2f> corners;
   RNG rng(12345);
   int r = 10;

   switch(mode){
      case USE_GOODFEATURE:
         dest = src.clone();
         mGoodFeatures(src, corners);
         /// Draw corners detected
         //std::cout<<"Number of corners detected: "<<corners.size()<<std::endl;
         
         for( int i = 0; i < corners.size(); i++ ){
            circle( dest, corners[i], r, Scalar(0, 255, 120), -1, 8, 0 );
         }

         /// Show what you got
         /*namedWindow("GoodFeaturesToTrack", WINDOW_NORMAL );
         imshow("GoodFeaturesToTrack", dest);*/
         break;

      default:
         std::cout<<"Wrong print mode on Points.print().";
   }
}

// Return the saliency map accordingly to the desired method
void Points::getPoints(PointsMethod method, Mat &src, std::vector<Point2f> &corners){

   switch(method){
      case GOODFEATURE:
         mGoodFeatures(src, corners);
         break;

      default:
         std::cout<<"Wrong saliency method on Points.getPoints.";
   }
}

void Points::mGoodFeatures(Mat &src, std::vector<Point2f> &corners){
   /* Medium  settings 
   int maxCorners = (src.cols * src.rows)*0.001;// 0.1% of the number of pixels - old: 1000;
   int blockSize = 6; // medium = 6 - high res = 3
   int distFraction = 40;// medium = 40 - high resolution = 60 Divides the minimum side, lower means a greater minimum distance
   double minDistance = (src.cols > src.rows)? src.rows/distFraction :src.cols/distFraction;//minimal distance beatween points
   double qualityLevel = 0.015;// 0.015 Define the minimal accepted quality of image corners. min = qualityLevel*bestCorner - Higher means less corners
   double k = 0.04; // 0.04
   bool useHarrisDetector = false;
   */
   // Favourite settings max corners 0.05% of n pixels - blocksize = 6 - min fraction = 60 - qualityLevel = 0.00501 or 0.00701 - k =0.04
   int maxCorners = (src.cols * src.rows)*0.0005;// 0.1% of the number of pixels - old: 1000;
   int blockSize = 6; // medium = 6 - high res = 3
   int distFraction = 100;// medium = 40 - high resolution = 60 Divides the minimum side, lower means a greater minimum distance
   double minDistance = (src.cols > src.rows)? src.rows/distFraction :src.cols/distFraction;//minimal distance beatween points
   double qualityLevel = 0.00501;// 0.015 Define the minimal accepted quality of image corners. min = qualityLevel*bestCorner - Higher means less corners
   double k = 0.04; // 0.04
   bool useHarrisDetector = false;
   
   // Copy the source image
   Mat copy, src_gray;
   copy = src.clone();

   cvtColor( src, src_gray, COLOR_BGR2GRAY );

   /// Apply corner detection
   goodFeaturesToTrack( src_gray,
      corners,
      maxCorners,
      qualityLevel,
      minDistance,
      Mat(),
      blockSize,
      useHarrisDetector,
      k);

   // Add points on the image corners - have to use limit-1 to avoid conflict on triangulation
   corners.push_back(Point2f(0,0));
   corners.push_back(Point2f(0,src.rows-1));
   corners.push_back(Point2f(src.cols-1,0));
   corners.push_back(Point2f(src.cols-1,src.rows-1));
   // Just so I know
   //std::cout<<"Number of corners detected: "<<corners.size()<<std::endl;
}