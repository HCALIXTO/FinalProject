#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <cmath>
#include <vector>
#include <iomanip>
#include <string>
#include <sstream>

using namespace cv;

class Colour{
private:
   Scalar colour;
   Scalar getTheAvarageColour(Mat &img, std::vector<Point> samples);
public:
   Colour ();
   Colour (Scalar c);
   void setColour(Scalar c);
   void setColour(Mat &img, std::vector<Point> samples);
   Scalar getColour();
   std::string getHexColour();
   bool compare(Colour c, int threshold);
   // Change the colourspace
   void convertImgToLAB(const Mat & source, Mat & destine);
   // Colour operations
   void toLAB(const Mat& inImage, Mat& outImage);
   bool thresholdScalar(Scalar a, Scalar b, float threshold);
   // Colour reduction functions
   Vec3b quantitizePixel(Vec3b color, uchar maskBit);
   void quantizeImage(const Mat& inImage, Mat& outImage, int numBits);
   void quantizeLABImage(const Mat& inImage, Mat& outImage, int colourBits, int lumiBits);
   void mKmeans(const Mat& inImage, Mat& outImage);
};

Colour::Colour (){
   setColour(Scalar(0,0,0,0));
}

Colour::Colour (Scalar c){
   setColour(c);
}

// Set the colour as a Scalar object
void Colour::setColour(Scalar c){
   colour = c;
}

// Set the colour as the avarage of samples from an img
void Colour::setColour(Mat &img, std::vector<Point> samples){
   setColour(getTheAvarageColour(img, samples));
}

// Return the Colour as a Scalar object
Scalar Colour::getColour(){
   return colour;
}

// Returns the colour in HexCode
std::string Colour::getHexColour(){
   Scalar colour = getColour();
   // Transforming the colour into hex
   std::stringstream cor;
   cor << "#";
   cor << std::hex << std::uppercase << std::setw(2) << std::setfill('0') << int(colour[2]);
   cor << std::hex << std::uppercase << std::setw(2) << std::setfill('0') << int(colour[1]);
   cor << std::hex << std::uppercase << std::setw(2) << std::setfill('0') << int(colour[0]);
   return cor.str();
}

// Compare two colours according to a threshold and return true if the difference is greater than the threshold
bool Colour::compare(Colour c, int threshold){
   return thresholdScalar(getColour(), c.getColour(), threshold);
}

// Convert an image to LAB colour space
void Colour::convertImgToLAB(const Mat & source, Mat & destine){
   // If the bit depth is not 8-bit unsigned I'll convert to it
   if(source.depth() != CV_8U){
      source.convertTo(source, CV_8U);//, 1.0/65535.0f
   }
   cvtColor(source, destine, CV_BGR2Lab);
}

// Public functions 

// Colour reduction functions "quantitize the img to number of bits" 
Scalar Colour::getTheAvarageColour(Mat &img, std::vector<Point> samples){
   Scalar sum(0,0,0,0);
   if(samples.size() > 0){
      for(int i = 0; i <samples.size(); i++){
         Point pt = samples.at(i);
         if(pt.y >= 0 && pt.y <= img.rows && pt.x >= 0 && pt.x <= img.cols){
            //std::cout<<"Sampled Point Col: "<<pt.x<<" | Row :"<<pt.y<<"\n"; 
            Scalar colour = img.at<Vec3b>(pt.y, pt.x);//(Row,Col);
            for(int c = 0; c < img.channels(); c++){
               sum[c] += colour[c];
            }
         }
      }
      for(int c = 0; c < img.channels(); c++){
         sum[c] /= samples.size();
      }
   }
   return sum;
}

// Compare the euclidian distance beetween two scalar colour with a threshold
bool Colour::thresholdScalar(Scalar a, Scalar b, float threshold){
   float dif = 0;
   for(int i = 0; i < 4; i++){
      dif = dif + std::pow((a[i] - b [i]), 2.0);
   }
   dif = std::pow(dif, (1/2.0));
   return dif > threshold;
}

// Quantitize a pixel colour
Vec3b Colour::quantitizePixel(Vec3b color, uchar maskBit){
   color[0] = color[0] & maskBit;
   color[1] = color[1] & maskBit;
   color[2] = color[2] & maskBit;
   return color;
}

// https://stackoverflow.com/questions/5906693/how-to-reduce-the-number-of-colors-in-an-image-with-opencv
// Quantitize a image
void Colour::quantizeImage(const Mat& inImage, Mat& outImage, int numBits){
   outImage = inImage.clone();

   uchar maskBit = 0xFF;// Make all the bits 1

   // keep numBits as 1 and (8 - numBits) would be all 0 towards the right
   maskBit = maskBit << (8 - numBits);

   for(int j = 0; j < outImage.rows; j++){
      for(int i = 0; i < outImage.cols; i++)
      {
         Vec3b valVec = outImage.at<Vec3b>(j, i);// Get the pixel's colour values
         outImage.at<Vec3b>(j, i) = quantitizePixel(valVec, maskBit);
      }
   }
}

// Change the colour space to LAB
void Colour::toLAB(const Mat& inImage, Mat& outImage){
   // If the bit depth is not 8-bit unsigned I'll convert to it
   if(inImage.depth() != CV_8U){
      inImage.convertTo(inImage, CV_8U);//, 1.0/65535.0f
   }
   cvtColor(inImage, outImage, CV_BGR2Lab);
}

// Quantitize a LAB image (is different because we need a differente qaunt val for L)
void Colour::quantizeLABImage(const Mat& inImage, Mat& outImage, 
                              int colourBits=-1, int lumiBits=-1){
   toLAB(inImage, outImage);
   // Define the level of reduction for the colour and luminesence
   if(colourBits < 0){
      colourBits = 2;
   }
   if(lumiBits < 0){
      lumiBits = colourBits + 2;// The lumiBits should be more detalied so I decided +2
   }

   uchar lumiBit = 0xFF;// Make all the bits 1
   uchar colourBit = 0xFF;// Make all the bits 1

   // keep numBits as 1 and (8 - numBits) would be all 0 towards the right
   lumiBit = lumiBit << (8 - lumiBits);
   colourBit = colourBit << (8 - colourBits);

   for(int j = 0; j < outImage.rows; j++){
      for(int i = 0; i < outImage.cols; i++)
      {
         Vec3b valVec = outImage.at<Vec3b>(j, i);// Get the pixel's colour values
         valVec[0] = valVec[0] & lumiBit;
         valVec[1] = valVec[1] & colourBit;
         valVec[2] = valVec[2] & colourBit;
         outImage.at<Vec3b>(j, i) = valVec;
      }
   }
}

// Reduce the number of colour by using the Kmean method (really slow)
void Colour::mKmeans(const Mat& inImage, Mat& outImage){
   Mat samples(inImage.rows * inImage.cols, 3, CV_32F);
   /* I don't know what this loop does check with Niel*/
   for( int y = 0; y < inImage.rows; y++ )
      for( int x = 0; x < inImage.cols; x++ )
         for( int z = 0; z < 3; z++)
            samples.at<float>(y + x*inImage.rows, z) = inImage.at<Vec3b>(y,x)[z];// y + x*inImage.rows gives the 1D index based on 2D coordinates X Y
   

   int clusterCount = 15;
   Mat labels;
   int attempts = 5;
   Mat centers;
   kmeans(samples, clusterCount, labels, TermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 10000, 0.0001), 
         attempts, KMEANS_PP_CENTERS, centers );


   //Mat outImage( inImage.size(), inImage.type() );
   for( int y = 0; y < inImage.rows; y++ ){
      for( int x = 0; x < inImage.cols; x++ ){ 
         int cluster_idx = labels.at<int>(y + x*inImage.rows,0);
         outImage.at<Vec3b>(y,x)[0] = centers.at<float>(cluster_idx, 0);
         outImage.at<Vec3b>(y,x)[1] = centers.at<float>(cluster_idx, 1);
         outImage.at<Vec3b>(y,x)[2] = centers.at<float>(cluster_idx, 2);
      }
   }
}