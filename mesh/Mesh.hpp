#include <stdio.h>
#include <vector>
#include <set>
#include <map>
#include <utility>
#include <math.h>
#include <opencv2/opencv.hpp>

#ifndef C_POLY
#define C_POLY
   #include "Polygon.hpp"
#endif

#ifndef C_EDGE
#define C_EDGE
   #include "Edge.hpp"
#endif

# define PI 3.14159265

using namespace cv;



// Print Functions
static void draw_point( Mat& img, Point2f fp, Scalar color );
static void draw_delaunay( Mat& img, Subdiv2D& subdiv, 
                           Scalar delaunay_color );
static void draw_voronoi( Mat& img, Subdiv2D& subdiv );

class Mesh {
   private:
      // point p - map[p.x][p.y] = trinagles(containing p) id vector
      //std::map<int, std::map<int, std::vector<int>> map;> pointIndex;
      std::map<std::pair<int, int>, std::set<int> > pointIndex;
      std::vector<Polygon> polyList;
      std::vector<Edge> edgeList;

      void mDelaunay(Subdiv2D &sdiv, std::vector<Point2f> &points, 
                                    Mat &inImage, int sampleSize);
      void addToPointIndex(Polygon poly);
      void addToPointIndex(int x, int y, int polyId);

      void removeFromPointIndex(Polygon poly);
      void removeFromPointIndex(int x, int y, int polyId);

      std::vector<int> getPolyNeighbours(Edge ed);
      std::set<int> getPtsPolygons(int x, int y);
      void printPointIndex();

      void translateToMesh(Subdiv2D &sdiv,Mat &inImage);

      std::vector<int> setIntersection(std::set<int> set1, 
                                       std::set<int> set2);
      
      Point getOtherPoint(Polygon poly, Edge ed);
      Edge newEdgeFromTriangles(Polygon p1, Polygon p2, Edge ed);
      int angleABC(Point a, Point b, Point c);
      bool isCrossing(const Point p1, const Point p2, 
                      const Point q1, const Point q2);
   public:
      enum PrintMode {USE_DELAUNAY, USE_VORONOI};
      enum MeshMethod {DELAUNAY};
      // Maybe should be private but for now go with it
      void rotateEdges(std::vector<int> indexes, Mat& inImage);
      bool rotateEdge(int index, Mat& inImage);
      
      Mesh();
      void print(PrintMode mode, Subdiv2D &sdiv, Mat& img);
      void print(Mat& img);
      void triangulate(MeshMethod method, 
                  std::vector<Point2f> &points, Mat &inImage);
      int getNumPolys();
      int getNumEdges();
      std::vector<Polygon> getPolygons();

      void addNoise();
      void testEdgeRotation(Mat& inImage);

      ~Mesh();
};

// Class constructor
Mesh::Mesh(){
}

// Class destructor
Mesh::~Mesh(){
   //std::cout<<"\nGoodbye...\n";
}

// Just to test the structures connections
void Mesh::addNoise(){
   Scalar color;
   for(int i = 0; i < getNumPolys()/10; i++){
      Edge ed = edgeList[rand() & getNumEdges()];
      std::vector<int> polys = getPolyNeighbours(ed);
      if(polys.size() == 2){
         
         color[0] = rand() & 255;
         color[1] = rand() & 255;
         color[2] = rand() & 255;

         polyList[polys[0]].setColour(color);
         
         color[0] = rand() & 255;
         color[1] = rand() & 255;
         color[2] = rand() & 255;
         
         polyList[polys[0]].setColour(color);
      }
      
   }
}
// TEst Edge rotation
void Mesh::testEdgeRotation(Mat& inImage){

   namedWindow("TEST", WINDOW_NORMAL );
   imshow("TEST", inImage);
   /*
   std::cout<<angleABC(Point(10,10), Point(5,5), Point(5,10))<<"\n"; // 45
   std::cout<<angleABC(Point(10,5), Point(5,5), Point(5,10))<<"\n"; // 90
   std::cout<<angleABC(Point(10,0), Point(5,5), Point(5,10))<<"\n"; // 135
   std::cout<<angleABC(Point(5,0), Point(5,5), Point(5,10))<<"\n"; // 180
   std::cout<<angleABC(Point(0,0), Point(5,5), Point(5,10))<<"\n"; // 225
   std::cout<<angleABC(Point(5,10), Point(5,5), Point(0,5))<<"\n"; // 270 / -90
   
   Point a1 = Point(10,5);
   Point a2 = Point(0,5);
   Point b1 = Point(1,0);
   Point b2 = Point(9,8);
   std::cout<<"Crossing test: "<<isCrossing(a1,a2, b1, b2)<<"\n";// 0 or 1
   */
   // getNumPolys()*1.8 - num of rotations | -1 random edge
   std::vector<int> rotations(getNumPolys()*0.6, -1);
   rotateEdges(rotations, inImage);

}

// Rotate multiple edges by index (or random if index -1)
void Mesh::rotateEdges(std::vector<int> indexes, Mat& inImage){
   for(int i = 0; i < indexes.size(); i++){
      if(indexes[i] < 0 || indexes[i] >= getNumEdges()){
         int attemps = 0;
         int index;
         bool response = false;
         while(!response && attemps < 10){
            index = rand() & (getNumEdges()-1);
            response = rotateEdge(index, inImage);
            attemps ++;
         }
      }else{
         rotateEdge(indexes[i], inImage);
      }
      
   }
}

// Rotate an edge by it's index (or random if index -1)
bool Mesh::rotateEdge(int index, Mat& inImage){
   

   Edge eOld = edgeList[index];
   std::vector<int> polys = getPolyNeighbours(eOld);
   if(polys.size() == 2){
      Polygon p1 = polyList[polys[0]];
      Polygon p2 = polyList[polys[1]];

      removeFromPointIndex(p1);
      removeFromPointIndex(p2);

      Edge eNew = newEdgeFromTriangles(p1, p2, eOld);
      // I can only rotate crossing edges (to avoid asa delta triangles)
      if(isCrossing(eOld.getP1(), eOld.getP2(), 
                    eNew.getP1(), eNew.getP2())){
         edgeList[index] = eNew;

         std::vector<Point> pts1;
         pts1.push_back(eOld.getP1());
         pts1.push_back(eNew.getP1());
         pts1.push_back(eNew.getP2());

         Polygon newP1 (pts1);
         newP1.setId(p1.getId());
         newP1.setColour(inImage, 100);
         //std::cout<<"T - "<<p1.getId()<<" Colour: "<<newP1.getColour()<<" \n";
         polyList[p1.getId()] = newP1;
         addToPointIndex(newP1);
         
         std::vector<Point> pts2;
         pts2.push_back(eOld.getP2());
         pts2.push_back(eNew.getP1());
         pts2.push_back(eNew.getP2());

         Polygon newP2 (pts2);
         newP2.setId(p2.getId());
         newP2.setColour(inImage, 100);
         //std::cout<<"T - "<<p2.getId()<<" Colour: "<<newP2.getColour()<<" \n";
         polyList[p2.getId()] = newP2;
         addToPointIndex(newP2);

         return true;
      }
   }
   return false;
   
}
// Return the point from a triangle that don't belong to an Edge
Point Mesh::getOtherPoint(Polygon poly, Edge ed){
   if(poly.numberOfPoints() == 3){
      std::vector<Point> points = poly.getPoints();
      Point pt1 = ed.getP1();
      Point pt2 = ed.getP2();
      for(int i = 0; i < points.size(); i++){
         if((points[i].x != pt1.x || points[i].y != pt1.y) &&
            (points[i].x != pt2.x || points[i].y != pt2.y)){
            return points[i];
         }
      }
   }
   return Point(0,-1);// This signs an error
}

// Return a new esdge from the point those triangles dont share
Edge Mesh::newEdgeFromTriangles(Polygon p1, Polygon p2, Edge ed){
   Point pt1 = getOtherPoint(p1, ed);
   Point pt2 = getOtherPoint(p2, ed);

   Edge newEd(pt1, pt2);
   return newEd;
}

// Return the angle between three points A B C
int Mesh::angleABC(Point a, Point b, Point c){
   Point ab = Point(b.x - a.x, b.y - a.y);
   Point cb = Point(b.x - c.x, b.y - c.y);

   float dot = (ab.x * cb.x + ab.y * cb.y); // dot product
   float cross = (ab.x * cb.y + ab.y * cb.x); // cross product
   float alpha = atan2(cross, dot);

   return (int) floor((alpha*180.00) / PI);// + 0.5
}

bool Mesh::isCrossing(const Point p1, const Point p2, 
                      const Point q1, const Point q2) {
   int oneSide = ((q1.x-p1.x)*(p2.y-p1.y) - (q1.y-p1.y)*(p2.x-p1.x))
               * ((q2.x-p1.x)*(p2.y-p1.y) - (q2.y-p1.y)*(p2.x-p1.x));

   int otherSide = ((p1.x-q1.x)*(q2.y-q1.y) - (p1.y-q1.y)*(q2.x-q1.x))
               * ((p2.x-q1.x)*(q2.y-q1.y) - (p2.y-q1.y)*(q2.x-q1.x));
    return (oneSide < 0) && (otherSide < 0);
}

// Print the base image accordingly to the selected mode
void Mesh::print(PrintMode mode, Subdiv2D &sdiv, Mat& img){
   Scalar color;
   color[0] = rand() & 255;
   color[1] = rand() & 255;
   color[2] = rand() & 255;

   switch(mode){
      case USE_DELAUNAY:
         /// Print the delaunay
         //std::cout<<"Print Time"<<"\n";
         draw_delaunay(img, sdiv, color);
         break;

      case USE_VORONOI:
         /// Print the voronoi
         draw_voronoi(img, sdiv);
         break;

      default:
         std::cout<<"Wrong print mode on Mesh.print().";
   }
}

// Render the mesh in a Mat object
void Mesh::print(Mat& outImage){
   for(int i = 0; i < polyList.size(); i++){
      polyList[i].draw(outImage);
   }
}

// Return the number of polygons in the mesh
int Mesh::getNumPolys(){
   return polyList.size();
}

std::vector<Polygon> Mesh::getPolygons(){
   return polyList;
}

// Return the number of edges in the mesh
int Mesh::getNumEdges(){
   return edgeList.size();
}

// Print the point Index structure
void Mesh::printPointIndex(){
   std::map<std::pair<int, int>, std::set<int> >::const_iterator it = pointIndex.begin();
   for( ; it != pointIndex.end(); it++){
      std::cout << it->first.first << "-" << it->first.second << ": " << "\n";
      std::cout<<"    ";
      for(std::set<int>::const_iterator i = it->second.begin(); 
                                    i != it->second.end(); ++i){
         std::cout<<*i<<" ";
      }
      std::cout<<"\n";
   }
}

void Mesh::mDelaunay(Subdiv2D &sdiv, std::vector<Point2f> &points, 
                     Mat &inImage, int sampleSize = 100){
   sdiv.insert(points);// Throw error -211 if one of the points is out of imgs boundaries
   /* For debugging 
   for (int i = 0; i < points.size(); ++i)
   {
      std::cout<<"Pt: "<<points[i]<<"\n";
      sdiv.insert(points[i]);
   }
   */
   translateToMesh(sdiv, inImage);
}

// translate a sdiv into the mesh data structute
void Mesh::translateToMesh(Subdiv2D &sdiv,Mat &inImage){
   // Fill the polyList and pointIndex
   std::vector<Vec6f> triangleList;
   sdiv.getTriangleList(triangleList);
   for( int i = 0; i < triangleList.size(); i++ ){
      // Fill the polyList
      Polygon poly;
      poly.setPoints(triangleList[i]);
      poly.setColour(inImage, 100);
      poly.setId(i);
      polyList.push_back(poly);
      // Fill the pointIndex
      addToPointIndex(poly);
   }
   //printPointIndex();
   // Fill the edgeList using the previous structure
   std::vector< Vec4f > edges;
   sdiv.getEdgeList(edges);
   for( int e = 0; e < edges.size(); e++ ){
      Edge ed(edges[e]); // Set a new edge obj

      edgeList.push_back(ed);
   }
}

// Add a polygon to the point index
void Mesh::addToPointIndex(Polygon poly){
   std::vector<Point> pts = poly.getPoints();
   for(int i = 0; i < pts.size(); i++){
      addToPointIndex(pts[i].x, pts[i].y, poly.getId());
   }
}

// Add a point coordinate - triangle ID to pointIndex
void Mesh::addToPointIndex(int x, int y, int polyId){
   std::pair<int, int> coordinate = std::make_pair(x, y);
   // Add the polyId to the point's list of polygons it is part of
   if(pointIndex.find(coordinate) != pointIndex.end()){
      pointIndex[coordinate].insert(polyId);
   }else{// If it is the first time I'll create the triangle list
      std::set<int> pointPolys;
      pointPolys.insert(polyId);
      pointIndex[coordinate] = pointPolys;
   }
}

// Remove a polygon to the point index
void Mesh::removeFromPointIndex(Polygon poly){
   std::vector<Point> pts = poly.getPoints();
   for(int i = 0; i < pts.size(); i++){
      removeFromPointIndex(pts[i].x, pts[i].y, poly.getId());
   }
}

// Remove a point coordinate - triangle ID to pointIndex
void Mesh::removeFromPointIndex(int x, int y, int polyId){
   std::pair<int, int> coordinate = std::make_pair(x, y);
   // Add the polyId to the point's list of polygons it is part of
   if(pointIndex.find(coordinate) != pointIndex.end()){
      pointIndex[coordinate].erase(polyId);
   }
}

std::vector<int> Mesh::getPolyNeighbours(Edge ed){
   Point p1 = ed.getP1();
   Point p2 = ed.getP2();
   std::set<int> p1Polys = getPtsPolygons(p1.x,p1.y);
   std::set<int> p2Polys = getPtsPolygons(p2.x,p2.y);

   return setIntersection(p1Polys,p2Polys);
}

// Return a list with all the polygons a point is part of
std::set<int> Mesh::getPtsPolygons(int x, int y){
   std::pair<int, int> coordinate = std::make_pair(x, y);
   if(pointIndex.find(coordinate) != pointIndex.end()){
      return pointIndex[coordinate];
   }else{// If it is the first time I'll create the triangle list
      std::set<int> pointPolys;
      return pointPolys;
   }
}

// Returns a vector with the union of two sets (to get the polygons with two points)
std::vector<int> Mesh::setIntersection(std::set<int> set1, std::set<int> set2){
   int size = (set1.size() > set2.size()) ? set1.size() : set2.size();
   std::vector<int> uni(size);
   std::vector<int>::iterator it;
   //if(set1.begin() != set1.end() && set2.begin() != set2.end())
   it = std::set_intersection (set1.begin(), set1.end(), set2.begin(), set2.end(), uni.begin());
   uni.resize(it-uni.begin());
   return uni;
}

// Return the triangulation according to the points provided
void Mesh::triangulate(MeshMethod method, 
                  std::vector<Point2f> &points, Mat &inImage){
   Size size = inImage.size();
   Rect rect(0, 0, size.width, size.height);
   Subdiv2D sdiv(rect);

   switch(method){
      case DELAUNAY:
         mDelaunay(sdiv, points, inImage);
         break;

      default:
         std::cout<<"Wrong triangulation method on Mesh.triangulate.";
   }
}

// Draw a single point
static void draw_point( Mat& img, Point2f fp, Scalar color ){
   circle( img, fp, 2, color, CV_FILLED, CV_AA, 0 );
}

// Draw delaunay triangles
static void draw_delaunay( Mat& img, Subdiv2D& subdiv, Scalar delaunay_color ){
   std::vector<Vec6f> triangleList;
   subdiv.getTriangleList(triangleList);
   std::vector<Point> pt(3);
   Size size = img.size();
   Rect rect(0,0, size.width, size.height);

   for( size_t i = 0; i < triangleList.size(); i++ ){
      Vec6f t = triangleList[i];
      pt[0] = Point(cvRound(t[0]), cvRound(t[1]));
      pt[1] = Point(cvRound(t[2]), cvRound(t[3]));
      pt[2] = Point(cvRound(t[4]), cvRound(t[5]));

      // Draw rectangles completely inside the image.
      if ( rect.contains(pt[0]) && rect.contains(pt[1]) && rect.contains(pt[2])){
         line(img, pt[0], pt[1], delaunay_color, 1, CV_AA, 0);
         line(img, pt[1], pt[2], delaunay_color, 1, CV_AA, 0);
         line(img, pt[2], pt[0], delaunay_color, 1, CV_AA, 0);
      }
   }
   namedWindow("DELAUNAY", WINDOW_NORMAL );
   imshow("DELAUNAY", img);
}

//Draw voronoi diagram
static void draw_voronoi( Mat& img, Subdiv2D& subdiv ){
   std::vector<std::vector<Point2f> > facets;
   std::vector<Point2f> centers;
   subdiv.getVoronoiFacetList(std::vector<int>(), facets, centers);

   std::vector<Point> ifacet;
   std::vector<std::vector<Point> > ifacets(1);

   for( size_t i = 0; i < facets.size(); i++ ){
      ifacet.resize(facets[i].size());
      for( size_t j = 0; j < facets[i].size(); j++ )
        ifacet[j] = facets[i][j];

      Scalar color;
      color[0] = rand() & 255;
      color[1] = rand() & 255;
      color[2] = rand() & 255;
      fillConvexPoly(img, ifacet, color, 8, 0);

      ifacets[0] = ifacet;
      polylines(img, ifacets, true, Scalar(), 1, CV_AA, 0);
      circle(img, centers[i], 3, Scalar(), CV_FILLED, CV_AA, 0);
   }
   namedWindow("VORONOI", WINDOW_NORMAL );
   imshow("VORONOI", img);
}
