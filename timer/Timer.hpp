#include <iostream>

class Timer
{
   double start_time;
public:
   void start();
   double stop();
};

// starts the timer
void Timer::start(){
   start_time = double(getTickCount());
}

// stops the timer and return the elapsed time in milesecconds
double Timer::stop(){
   double stop_time = double(getTickCount());
   return (stop_time - start_time)* 1000 / getTickFrequency();
}