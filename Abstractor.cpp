#include <stdio.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>

#ifndef C_SALIENCY
#define C_SALIENCY
   #include "Saliency.cpp"
#endif
#ifndef C_POINTS
#define C_POINTS
   #include "Points.cpp"
#endif
#ifndef C_MESH
#define C_MESH
   #include "Mesh.cpp"
#endif
#ifndef C_COLOUR
#define C_COLOUR
   #include "Colour.cpp"
#endif
#ifndef C_INTERPRETER
#define C_INTERPRETER
   #include "Interpreter.cpp"
#endif
#ifndef C_TIMER
#define C_TIMER
   #include "Timer.cpp"
#endif

#include "OpenCVDemo.cpp"

using namespace cv;

std::vector<std::string> parsePath(std::string path, char pathChar,
                                char extensionChar);

int main(int argc, char** argv )
{
    if ( argc < 2 )
    {
        printf("usage: abstractor.out <Image_Path>\n");
        return -1;
    }
    std::string ImgPath = argv[1];
    std::vector<std::string> parsedImgPath = parsePath(ImgPath,'/','.');
    
    Interpreter interp;
    Timer clk;
    Mat image, labImg;
    image = imread( ImgPath, CV_LOAD_IMAGE_COLOR );

    if ( !image.data ){
        printf("No image data \n");
        return -1;
    }

    if( argc == 3){
        Mat compressed;
        compressed = imread( argv[2], CV_LOAD_IMAGE_COLOR );
        // resize the compressed img to match the source img (if I don't i can't run the quality checks)
        resize(compressed, compressed, image.size());

        std::cout<<"Compressed PSNR: "<<interp.getPSNR(image, compressed)<<"\n";
        std::cout<<"Compressed root mean squared error: "<<interp.getRMSE(image, compressed)<<"\n";
        //Some times it frozes the pc
        //std::cout<<"Compressed structural similarity: "<<interp.getMSSIM(image, compressed)<<"\n";
    }
    // Simulated Annealing
    
    std::ofstream file( "../Stats/simulatedAnnealing.txt", std::ios::app);
    if ( file ){
        
        Mat simAnn = Mat::zeros(image.size(), image.type());
        clk.start();
        
        interp.simulatedAnnealing(image, simAnn, parsedImgPath[1], parsedImgPath[0], &file);
        
        file<< clk.stop() << std::endl;
        file.close();
    }
    /* */
    // Greedy search
    file.open( "../Stats/greedySearch.txt", std::ios::app);
    if ( file ){
        clk.start();

        Mat greedy = Mat::zeros(image.size(), image.type());
        interp.greedySearch(image, greedy, parsedImgPath[1], parsedImgPath[0], &file);
    
        file<< clk.stop() << std::endl;
        file.close();
    }

    /* DELAUNAY TEMPLATE

    Mat delaunay = Mat::zeros(image.size(), image.type());
    clk.start();
    interp.doTheDelaunay(image, delaunay, parsedImgPath[1], parsedImgPath[0]);
    std::cout<<"Time to make DELAUNAY: "<<clk.stop()<<" ms \n";
    namedWindow("DELAUNAY", WINDOW_NORMAL );
    imshow("DELAUNAY", delaunay);
    std::cout<<"DELAUNAY PSNR: "<<interp.getPSNR(image, delaunay)<<"\n";
    std::cout<<"DELAUNAY root mean squared error: "<<interp.getRMSE(image, delaunay)<<"\n";
    //Some times it frozes the pc
    //std::cout<<"DELAUNAY structural similarity: "<<interp.getMSSIM(image, delaunay)<<"\n";

    */

    //Resize the image if it is too big, otherwise will take forever to run
    //std::cout<<"Rows : "<<image.rows<<" | Cols: "<<image.cols<<"\n";
    /*if(image.rows > image.cols){
        if(image.rows > 1000){// no particular reson for 1000
            int nRows = 1000;
            int nCols = (1000.00/image.rows)*image.cols;
            resize(image, image, Size(nCols, nRows), 0, 0, INTER_NEAREST);
        }
    }else{
        if(image.cols > 1000){
            int nRows = (1000.00/image.cols)*image.rows;
            int nCols = 1000;
            resize(image, image, Size(nCols, nRows), 0, 0, INTER_NEAREST);
        }
    }*/
    //std::cout<<"Rows : "<<image.rows<<" | Cols: "<<image.cols<<"\n";
    
    /*
    OpenCVDemo demo;
    demo.experiment(image);
    */
    // Some fun img interpretation
    
    /*
    Mat floyd;
    interp.floydSteinberg(image, floyd, 1);
    namedWindow("Dithered", WINDOW_NORMAL );
    imshow("Dithered", floyd);
    */
    /* Fun with circles 
    
    // Maybe I can check if the current circle's colour differs from the circle in the background, if is less than threshold I don create it
    
    Mat circles = Mat::zeros(image.size(), image.type());;
    clk.start();
    interp.representWithCircles(image, circles, 1000, parsedImgPath[1], parsedImgPath[0]);
    std::cout<<"Time to make Circles: "<<clk.stop()<<" ms \n";
    namedWindow("CIRCLES", WINDOW_NORMAL );
    imshow("CIRCLES", circles);

    //std::cout<<"Circles PSNR: "<<interp.getPSNR(image, circles)<<"\n";
    //std::cout<<"Circles root mean squared error: "<<interp.getRMSE(image, circles)<<"\n";
    //std::cout<<"Circles structural similarity: "<<interp.getMSSIM(image, circles)<<"\n";

    */

    /*
    Mat rect = Mat::zeros(image.size(), image.type());;
    interp.representWithPolygon(image, rect, Interpreter::RECTANGLE, 250);
    namedWindow("RECTANGLES", WINDOW_NORMAL );
    imshow("RECTANGLES", rect);

    Mat tri = Mat::zeros(image.size(), image.type());;
    interp.representWithPolygon(image, tri, Interpreter::L_R, 250);
    namedWindow("TRIANGLES LR", WINDOW_NORMAL );
    imshow("TRIANGLES LR", tri);
    */
    /*
    Mat tri2 = Mat::zeros(image.size(), image.type());;
    interp.representWithPolygon(image, tri2, Interpreter::R_L, 250);
    namedWindow("TRIANGLES RL", WINDOW_NORMAL );
    imshow("TRIANGLES RL", tri2);
    */
    /*
    Mat tri3 = Mat::zeros(image.size(), image.type());;
    interp.representWithPolygon(image, tri3, Interpreter::X, 300);
    namedWindow("TRIANGLES X", WINDOW_NORMAL );
    imshow("TRIANGLES X", tri3);

    Mat tri4 = Mat::zeros(image.size(), image.type());;
    interp.representWithPolygon(image, tri4, Interpreter::DIAMOND, 300);
    namedWindow("DIAMOND", WINDOW_NORMAL );
    imshow("DIAMOND", tri4);

    Mat tri5 = Mat::zeros(image.size(), image.type());;
    interp.representWithPolygon(image, tri5, Interpreter::DIAMOND_H, 300);
    namedWindow("DIAMOND_H", WINDOW_NORMAL );
    imshow("DIAMOND_H", tri5);
    */
    /*
    Mat tri6 = Mat::zeros(image.size(), image.type());;
    interp.representWithPolygon(image, tri6, Interpreter::DIAMOND_V, 150);
    namedWindow("DIAMOND_V", WINDOW_NORMAL );
    imshow("DIAMOND_V", tri6);

    Mat tri7 = Mat::zeros(image.size(), image.type());;
    interp.representWithPolygon(image, tri7, Interpreter::DIAMOND_X, 150);
    namedWindow("DIAMOND_X", WINDOW_NORMAL );
    imshow("DIAMOND_X", tri7);

    Mat tri8 = Mat::zeros(image.size(), image.type());;
    interp.representWithPolygon(image, tri8, Interpreter::SUN, 150);
    namedWindow("SUN", WINDOW_NORMAL );
    imshow("SUN", tri8);
    
    Mat tri9 = Mat::zeros(image.size(), image.type());;
    interp.representWithPolygon(image, tri9, Interpreter::RAND, 150);
    namedWindow("RAND", WINDOW_NORMAL );
    imshow("RAND", tri9);
    */

    /* COLOUR REDUCTION */
    /*Colour cl;
     Quantitazation Demo 
    Mat reducedImg;
    cl.quantizeImage(image, reducedImg, 2);
    
    Mat labReduced;
    cl.quantizeLABImage(image, labReduced, 8, 8);*/
    /* Kmean Demo
    Mat imgKMean( image.size(), image.type() );
    cl.mKmeans(image, imgKMean);
    namedWindow("Kmean Img", WINDOW_NORMAL );
    imshow("Kmean Img", imgKMean);
    */
    
    /* SALIENCY */
    /* Exemple on how can I get the saliency map
    Saliency sal;
    sal.print(image, Saliency::USE_CANNY);
    
    Mat salMap;
    
    sal.getSaliency(Saliency::CANNY, salMap);
    
    namedWindow("Feature MAP", WINDOW_NORMAL );
    imshow("Feature MAP", salMap);
    */
    
    /* POINT POSITIONING 
    Points pt;
    std::vector<Point2f> points, points2;
    
    //pt.getPoints(Points::GOODFEATURE, image, points);
    pt.getPoints(Points::GOODFEATURE, labReduced, points2);
    //pt.print(Points::USE_GOODFEATURE, image);
    */
    /* MESH GENERATION 
    // Rectangle to be used with Subdiv2D
    Size size = image.size();
    Rect rect(0, 0, size.width, size.height);

    Mesh ms;
    //Subdiv2D sdiv(rect);
    Subdiv2D sdiv2(rect);
    //ms.triangulate(Mesh::DELAUNAY,sdiv, points);
    ms.triangulate(Mesh::DELAUNAY,sdiv2, points2);

    //ms.print(Mesh::USE_DELAUNAY,sdiv, image);
    ms.print(Mesh::USE_DELAUNAY,sdiv2, image);
    */
    waitKey(0);
    return 0;
}
// '\\' is equal \ in real life
std::vector<std::string> parsePath(std::string input, char pathChar = '\\',
                                char extensionChar = '.'){
    std::vector<std::string> list;
    int pathEnd = input.rfind(pathChar);
    int fileNameEnd = input.rfind(extensionChar);

    std::string path = input.substr(0, pathEnd+1);
    std::string fName = input.substr(pathEnd+1, fileNameEnd-pathEnd-1);

    list.push_back(path);
    list.push_back(fName);
    return list;
}
