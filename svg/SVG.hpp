#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

#ifndef C_CIRCLE
#define C_CIRCLE
   #include "Circle.hpp"
#endif

class SVG{
   std::stringstream strStream;
public:
   SVG(const Mat &img);
   void add(Polygon p);
   void add(std::vector<Polygon> polyList);
   void add(Circle c);
   void end();
   void save(std::string fileName, std::string path);
   //~SVG();
   
};

// Start the svg file seting it's dimensions to be like the image's
SVG::SVG(const Mat &img){
   int width = img.cols;
   int height = img.rows;

   strStream<<"<svg width=\""<<width<<"\" height=\""<<height<<
         "\" viewBox=\"0 0 "<<width<<" "<<height<<"\">"<<"<g>";
}

// Add a Polygon to the buffer
void SVG::add(Polygon poly){
   std::vector<Point> pts = poly.getPoints();

   strStream<<"<polygon points=\"";
   for(int i = 0; i<pts.size();i++){
      Point p = pts[i];
      strStream<<p.x<<","<<p.y<<" ";
   }
   strStream<<"\" fill=\""<<poly.getHexColour()<<"\"/>";

}

// Add a List of Polygons to the buffer
void SVG::add(std::vector<Polygon> polyList){
   for(int i = 0; i < polyList.size(); i++){
      add(polyList[i]);
   }
}

// Add a Circle to the buffer
void SVG::add(Circle c){
   Point center = c.getCenter();

   strStream<<"<circle";
   strStream<<" cx=\""<<center.x<<"\" cy=\""<<center.y<<"\"";
   strStream<<" r=\""<<c.getRadius()<<"\"";
   strStream<<" fill=\""<<c.getHexColour()<<"\"/>";

}

// Finish the svg file
void SVG::end(){
  strStream<<"</g></svg>";
}

// Save the svg file
void SVG::save(std::string fileName = "svgOut", std::string path = "images/"){
   std::string file;
   file += path;
   file += fileName;
   file += ".svg";
   std::cout<<file<<"\n";
   end();
   
   std::ofstream f( file.c_str() );//"images/calixto.svg");
   f << strStream.str() << std::endl;
   f.close();
   std::cout<<"Eh p ter salvo\n";
}