#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace cv;

class OpenCVDemo {
private:
   Mat labImg;
public:
   enum PrintMode {FULL, BY_CHANNEL};
   OpenCVDemo();
   void setBaseImg(Mat source);
   void print(PrintMode mode, Mat source);
   void mGoodFeaturesToTrack(Mat source);
   void mCornerHarris(Mat source);
   void mCanny(Mat source);
   void mLsd_lines(Mat source);
   void mHoughCircles(Mat source);
   void experiment(Mat source);
   ~OpenCVDemo();
};

// Class constructor
OpenCVDemo::OpenCVDemo(){
   
}

// Class destructor
OpenCVDemo::~OpenCVDemo(){
   std::cout<<"Goodbye...";
}

// Set the base image, get the source and change it's colourspace to LAB
void OpenCVDemo::setBaseImg(Mat source){
   // If the bit depth is not 8-bit unsigned I'll convert to it
   if(source.depth() != CV_8U){
      source.convertTo(source, CV_8U);//, 1.0/65535.0f
   }
   cvtColor(source, labImg, CV_BGR2Lab);
}

// Print the base image accordingly to the selected mode
void OpenCVDemo::print(PrintMode mode, Mat source){
   Mat ch[3];
   setBaseImg(source);

   switch(mode){
      case FULL:
         namedWindow("Full LAB Image", WINDOW_NORMAL );
         imshow("Full LAB Image", labImg);
         break;

      case BY_CHANNEL:
         split(labImg, ch);
         namedWindow("L Channel", WINDOW_NORMAL );
         imshow("L Channel", ch[0]);
         namedWindow("A Channel", WINDOW_NORMAL );
         imshow("A Channel", ch[1]);
         namedWindow("B Channel", WINDOW_NORMAL );
         imshow("B Channel", ch[2]);
         break;

      default:
         std::cout<<"Wrong print mode on OpenCVDemo.print().";
   }
}

// Locates corners and print the image with the found ones
void OpenCVDemo::mGoodFeaturesToTrack(Mat source){
   std::vector<Point2f> corners;
   int maxCorners = 1000;
   double qualityLevel = 0.01;
   double minDistance = (source.cols > source.rows)? source.rows/10 :source.cols/10;//100;
   int blockSize = 3;
   bool useHarrisDetector = false;
   double k = 0.04;
   RNG rng(12345);

   // Copy the source image
   Mat copy, src_gray;
   copy = source.clone();

   cvtColor( source, src_gray, COLOR_BGR2GRAY );

   /// Apply corner detection
   goodFeaturesToTrack( src_gray,
      corners,
      maxCorners,
      qualityLevel,
      minDistance,
      Mat(),
      blockSize,
      useHarrisDetector,
      k );

   // Add points on the image corners
   corners.push_back(Point2f(0,0));
   corners.push_back(Point2f(0,source.rows));
   corners.push_back(Point2f(source.cols,0));
   corners.push_back(Point2f(source.cols,source.rows));

   /// Draw corners detected
   std::cout<<"** Number of corners detected: "<<corners.size()<<std::endl;
   int r = 4;
   for( int i = 0; i < corners.size(); i++ ){
      circle( copy, corners[i], r, Scalar(rng.uniform(0,255), rng.uniform(0,255),
      rng.uniform(0,255)), -1, 8, 0 );
   }

   /// Show what you got
   namedWindow("GoodFeaturesToTrack", WINDOW_NORMAL );
   imshow("GoodFeaturesToTrack", copy);
}

// Finds corners in the image, need to fiddle with the parametres
void OpenCVDemo::mCornerHarris(Mat source){
   std::string const source_window = "Source image";
   std::string const corners_window = "Corners detected";
   Mat dst, dst_norm, dst_norm_scaled, src_gray;
   dst = Mat::zeros( source.size(), CV_32FC1 );
   cvtColor(source, src_gray, COLOR_BGR2GRAY);

   int thresh = 200;
   int max_thresh = 255;

   /// Detector parameters
   int blockSize = 2;
   int apertureSize = 3;
   double k = 0.04;

   /// Detecting corners
   cornerHarris( src_gray, dst, blockSize, apertureSize, k, BORDER_DEFAULT );

   /// Normalizing
   normalize( dst, dst_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat() );
   convertScaleAbs( dst_norm, dst_norm_scaled );

   /// Drawing a circle around corners
   for( int j = 0; j < dst_norm.rows ; j++ ){
      for( int i = 0; i < dst_norm.cols; i++ ){
         if( (int) dst_norm.at<float>(j,i) > thresh ){
            circle( dst_norm_scaled, Point( i, j ), 5,  Scalar(0), 2, 8, 0 );
         }
      }
   }
   /// Showing the result
   namedWindow( corners_window, WINDOW_NORMAL );
   imshow( corners_window, dst_norm );
}

// Canny edge detector
void OpenCVDemo::mCanny(Mat source){
   Mat dst;
   Canny( source, dst, 50, 200, 3 );
   /// Show what you got
   namedWindow("Canny", WINDOW_NORMAL );
   imshow("Canny", dst);
}

// Place lines near edges and creates a representation of the image using only lines
void OpenCVDemo::mLsd_lines(Mat source){
   Mat image;
   bool useRefine = true;
   bool useCanny = false;
   bool overlay = false;
   // Create and LSD detector with standard or no refinement.
   Ptr<LineSegmentDetector> ls = useRefine ? createLineSegmentDetector(LSD_REFINE_STD)
                                          : createLineSegmentDetector(LSD_REFINE_NONE);
   double start = double(getTickCount());
   std::vector<Vec4f> lines_std;

   cvtColor( source, image, COLOR_BGR2GRAY );

   if (useCanny){
      Canny(image, image, 50, 200, 3); // Apply Canny edge detector
   }

   // Detect the lines
   ls->detect(image, lines_std);

   double duration_ms = (double(getTickCount()) - start) * 1000 / getTickFrequency();
   std::cout << "It took " << duration_ms << " ms." << std::endl;

   // Show found lines
   if (!overlay || useCanny)
   {
      image = Scalar(0, 0, 0);
   }
   ls->drawSegments(image, lines_std);
   String window_name = useRefine ? "Result - standard refinement" : "Result - no refinement";
   window_name += useCanny ? " - Canny edge detector used" : "";
   namedWindow(window_name, WINDOW_NORMAL );
   imshow(window_name, image);
}

// Finds perfect circles in the image
void OpenCVDemo::mHoughCircles(Mat source){
   Mat img, gray;
   std::vector<Vec3f> circles;
   img = source.clone();
   cvtColor(img, gray, COLOR_BGR2GRAY);
   // smooth it, otherwise a lot of false circles may be detected
   GaussianBlur( gray, gray, Size(9, 9), 2, 2 );

   HoughCircles(gray, circles, HOUGH_GRADIENT,
                 2, gray.rows/4, 200, 100 );

   for( size_t i = 0; i < circles.size(); i++ ){
      Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
      int radius = cvRound(circles[i][2]);
      // draw the circle center
      circle( img, center, 3, Scalar(0,255,0), -1, 8, 0 );
      // draw the circle outline
      circle( img, center, radius, Scalar(0,0,255), 3, 8, 0 );
   }
   namedWindow( "circles", WINDOW_NORMAL);
   imshow( "circles", img );
}

// Run several methods and print everything
void OpenCVDemo::experiment(Mat source){
   mGoodFeaturesToTrack(source);
   mCanny(source);
   mLsd_lines(source);
   mHoughCircles(source);
   mCornerHarris(source);
}