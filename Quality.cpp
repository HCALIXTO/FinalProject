#include <stdio.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <dirent.h> // deals with path and files inside

#ifndef C_INTERPRETER
#define C_INTERPRETER
   #include "Interpreter.cpp"
#endif
#ifndef C_TIMER
#define C_TIMER
   #include "Timer.cpp"
#endif

#include "OpenCVDemo.cpp"


using namespace cv;

int main(int argc, char** argv )
{
   if ( argc < 2 ){
      printf("usage: quality <Test_Path>\n");
      return -1;
   }
   Interpreter interp;
   Timer clk;
   Mat img;

   std::string path = argv[1];

   DIR *dir;
   struct dirent *ent;
   if ((dir = opendir (argv[1])) != NULL) {
      std::string report = path+"/QualityReport.txt";
      std::ofstream file( report.c_str(),std::ofstream::out | std::ofstream::trunc); // "../Stats/QualityReport.txt"
      if(file){
         file<<"Name|ImgSize|Ntrangles|TimeToTriangulate|PSNR|Simi[0]|Simi[1]|Simi[2]"<< std::endl;
         while ((ent = readdir (dir)) != NULL) {
            std::string nm = ent->d_name;
            int endfName = nm.rfind('.');
            std::string fName = nm.substr(0, endfName);
            std::string ext = nm.substr(endfName+1, -1);
            std::cout<<fName<<"\n";
            if(ext == "jpg" || ext == "JPG" || ext == "png" || ext == "PNG" || ext == "jpeg" || ext == "JPEG"){
               img = imread( path+"/"+ent->d_name, CV_LOAD_IMAGE_COLOR );
               Mat delaunay = Mat::zeros(img.size(), img.type());

               file<<nm<<"|";
               file<<img.total() * img.elemSize()<<"|";

               clk.start();
               interp.doTheDelaunay(img, delaunay, fName+"", path+"/placeholders/", &file);
               file<< clk.stop()<<"|";
               file<< interp.getPSNR(img, delaunay)<<"|";
               Scalar mssim = interp.getMSSIM(img, delaunay);
               file<< mssim[0] <<"|";
               file<< mssim[1] <<"|";
               file<< mssim[2] <<"|";
               //std::cout<<fName<<" - "<<ext<<"\n";
               file<< std::endl;
            }
            /**/
         }
         file.close();
      }
      closedir (dir);
   } else {
      printf("Error: Could not open the provided path.\n");
      return -1;
   }

   
   return 0;
}













/*


*/