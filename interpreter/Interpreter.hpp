#include <stdio.h>
#include <sstream>
#include <opencv2/opencv.hpp>
#include <cmath>
#include <vector>
#include <string>

#ifndef C_POLY
#define C_POLY
   #include "Polygon.hpp"
#endif
#ifndef C_CIRCLE
#define C_CIRCLE
   #include "Circle.hpp"
#endif

#ifndef C_COLOUR
#define C_COLOUR
   #include "Colour.cpp"
#endif

#ifndef C_SALIENCY
#define C_SALIENCY
   #include "Saliency.cpp"
#endif
#ifndef C_POINTS
#define C_POINTS
   #include "Points.cpp"
#endif
#ifndef C_MESH
#define C_MESH
   #include "Mesh.cpp"
#endif
#ifndef C_SVG
#define C_SVG
   #include "SVG.cpp"
#endif

#ifndef C_TIMER
#define C_TIMER
   #include "Timer.cpp"
#endif


class Interpreter{
   double random(double x, double y);
   // Generate specific polygon types
   std::vector<Polygon> getRectangle(int lowCol,int upCol,
                                       int lowRow,int upRow);
   std::vector<Polygon> getLRTriangle(int lowCol,int upCol,
                                       int lowRow,int upRow);
   std::vector<Polygon> getRLTriangle(int lowCol,int upCol,
                                       int lowRow,int upRow);
   std::vector<Polygon> getXTriangle(int lowCol,int upCol,
                                       int lowRow,int upRow);
   std::vector<Polygon> getDiamond(int lowCol,int upCol,
                                       int lowRow,int upRow);
   std::vector<Polygon> getDiamondH(int lowCol,int upCol,
                                       int lowRow,int upRow);
   std::vector<Polygon> getDiamondV(int lowCol,int upCol,
                                       int lowRow,int upRow);
   std::vector<Polygon> getDiamondX(int lowCol,int upCol,
                                       int lowRow,int upRow);
   std::vector<Polygon> getSUN(int lowCol,int upCol,
                                       int lowRow,int upRow);

public:
   enum PolyType {RECTANGLE ,L_R, R_L, X, DIAMOND, DIAMOND_V, 
                  DIAMOND_H, DIAMOND_X, SUN, RAND};
   
   Interpreter();
   // Quality mesures
   double getMSE(const Mat& I1, const Mat& I2);
   double getPSNR(const Mat& I1, const Mat& I2);
   double getRMSE(const Mat& I1, const Mat& I2);
   Scalar getMSSIM( const Mat& i1, const Mat& i2);
   // Experiment
   void spreadPixels(Mat &inImage, Mat &outImage, int step, int numBits);
   // Floyd - Steinberg
   void floydSteinberg(Mat &inImage, Mat &outImage, int numBits);
   // Circles
   int circulate(Mat &inImage, Mat &outImage, int step,
            SVG &svg, float threshold);
   void representWithCircles(Mat &inImage, Mat &outImage, int numCircles,
                              std::string filename, std::string path,
                              float threshold ,float growFactor, 
                              int maxAttempts);
   // Polygons
   void representWithPolygon(Mat &inImage, Mat &outImage, PolyType pType,
                                    int numRect, float threshold, 
                                    float growFactor, int maxAttempts);
   int polygonate(Mat &inImage, Mat &outImage, PolyType pType,
                     int rowStep, int colStep, float threshold);

   void doTheDelaunay(Mat &inImage, Mat &outImage, 
                        std::string filename, std::string path,
                        std::ofstream* dataOut);
   Mesh makeDelaunayMesh(Mat &inImage, std::ofstream* dataOut);
   Mesh addEdgesToMesh(Mat &inImage, std::vector<Vec4f> edges,
                        std::ofstream* dataOut);
   void simulatedAnnealing(Mat &inImage, Mat &outImage, 
                        std::string filename, std::string path,
                        std::ofstream* dataOut);
   void greedySearch(Mat &inImage, Mat &outImage, 
                     std::string filename, std::string path,
                     std::ofstream* dataOut);
   // Generate polygons
   std::vector<Polygon> getPolygon(PolyType pType,int 
                                 lowCol,int upCol,int lowRow,int upRow);
   
};


double Interpreter::random(double x = 0.0, double y = 1.0){
   static bool first = true;
   double min = (x < y)? x : y;
   double max = (x > y)? x : y;

   if (first) {  
      srand( time(NULL) ); //seeding for the first time only!
      first = false;
   }
   return fmod(min + rand() , (( max + 1.0 ) - min));
}

// Public functions

Interpreter::Interpreter(){

}

// ERROR functions
// source : https://docs.opencv.org/2.4/doc/tutorials/highgui/video-input-psnr-ssim/video-input-psnr-ssim.html#image-similarity-psnr-and-ssim

// returns the mean squad error
double Interpreter::getMSE(const Mat& I1, const Mat& I2){
   Mat s1;
   absdiff(I1, I2, s1);       // |I1 - I2|
   s1.convertTo(s1, CV_32F);  // cannot make a square on 8 bits
   s1 = s1.mul(s1);           // |I1 - I2|^2

   Scalar s = sum(s1);         // sum elements per channel

   double sse = s.val[0] + s.val[1] + s.val[2]; // sum channels

   if( sse <= 1e-10){ // for small values return zero
      return 0;
   }else{
      double  mse =sse /(double)(I1.channels() * I1.total());
      return mse;
   }
}
// return the Peak signal-to-noise ratio of two images - BIGGER is better
// https://en.wikipedia.org/wiki/Peak_signal-to-noise_ratio
double Interpreter::getPSNR(const Mat& I1, const Mat& I2){
   double mse = getMSE(I1, I2);
   if(mse == 0){
      return 0;
   }else{
      double psnr = 10.0*log10((255*255)/mse);
      return psnr;
   }
}
// return the root mean squared error - LOWER is better
double Interpreter::getRMSE(const Mat& I1, const Mat& I2){
   double mse = getMSE(I1, I2);
   return std::pow(mse,0.5);
}

// return the structural similarity, it is ment to be more consistent with the 
// human eye perception, check:Z. Wang, A. C. Bovik, H. R. Sheikh and E. P. Simoncelli,
// “Image quality assessment: From error visibility to structural similarity,”
// https://en.wikipedia.org/wiki/Structural_similarity
// source: https://docs.opencv.org/2.4/doc/tutorials/highgui/video-input-psnr-ssim/video-input-psnr-ssim.html#image-similarity-psnr-and-ssim
Scalar Interpreter::getMSSIM( const Mat& i1, const Mat& i2){
   const double C1 = 6.5025, C2 = 58.5225;
   /***************************** INITS **********************************/
   int d     = CV_32F;

   Mat I1, I2;
   i1.convertTo(I1, d);           // cannot calculate on one byte large values
   i2.convertTo(I2, d);

   Mat I2_2   = I2.mul(I2);        // I2^2
   Mat I1_2   = I1.mul(I1);        // I1^2
   Mat I1_I2  = I1.mul(I2);        // I1 * I2

   /***********************PRELIMINARY COMPUTING ******************************/

   Mat mu1, mu2;   //
   GaussianBlur(I1, mu1, Size(11, 11), 1.5);
   GaussianBlur(I2, mu2, Size(11, 11), 1.5);

   Mat mu1_2   =   mu1.mul(mu1);
   Mat mu2_2   =   mu2.mul(mu2);
   Mat mu1_mu2 =   mu1.mul(mu2);

   Mat sigma1_2, sigma2_2, sigma12;

   GaussianBlur(I1_2, sigma1_2, Size(11, 11), 1.5);
   sigma1_2 -= mu1_2;

   GaussianBlur(I2_2, sigma2_2, Size(11, 11), 1.5);
   sigma2_2 -= mu2_2;

   GaussianBlur(I1_I2, sigma12, Size(11, 11), 1.5);
   sigma12 -= mu1_mu2;

   ///////////////////////////////// FORMULA ////////////////////////////////
   Mat t1, t2, t3;

   t1 = 2 * mu1_mu2 + C1;
   t2 = 2 * sigma12 + C2;
   t3 = t1.mul(t2);              // t3 = ((2*mu1_mu2 + C1).*(2*sigma12 + C2))

   t1 = mu1_2 + mu2_2 + C1;
   t2 = sigma1_2 + sigma2_2 + C2;
   t1 = t1.mul(t2);               // t1 =((mu1_2 + mu2_2 + C1).*(sigma1_2 + sigma2_2 + C2))

   Mat ssim_map;
   divide(t3, t1, ssim_map);      // ssim_map =  t3./t1;

   Scalar mssim = mean( ssim_map ); // mssim = average of ssim map
return mssim;
}

// Representation functions

//floyd steinberg algorithm
// inspiration: http://www.tannerhelland.com/4660/dithering-eleven-algorithms-source-code/
void Interpreter::floydSteinberg(Mat &inImage, Mat &outImage, int numBits){
   Colour cl;
   Mat img = inImage.clone();
   outImage = img.clone();
   uchar maskBit = 0xFF;// Make all the bits 1
   // keep numBits as 1 and (8 - numBits) would be all 0 towards the right
   maskBit = maskBit << (8 - numBits);
   for(int i = 0; i < img.rows; i++ ){
      for(int j = 0; j < img.cols; j++ ){
         Vec3b newpixel = cl.quantitizePixel(img.at<Vec3b>(i,j), maskBit);
         outImage.at<Vec3b>(i,j) = newpixel;

         for(int k=0;k<img.channels();k++){
            int quant_error = (int)img.at<Vec3b>(i,j)[k] - newpixel[k];
            if(j+1<img.cols)
               img.at<Vec3b>(i,j+1)[k] = fmin(255,fmax(0,(int)img.at<Vec3b>(i,j+1)[k] + (7 * quant_error) / 16));
            if(i+1 < img.rows && j-1 >= 0)
               img.at<Vec3b>(i+1,j-1)[k] = fmin(255,fmax(0,(int)img.at<Vec3b>(i+1,j-1)[k] + (3 * quant_error) / 16));
            if(i+1 < img.rows)
               img.at<Vec3b>(i+1,j)[k] = fmin(255,fmax(0,(int)img.at<Vec3b>(i+1,j)[k] + (5 * quant_error) / 16));
            if(i+1 < img.rows && j+1 < img.cols)
               img.at<Vec3b>(i+1,j+1)[k] = fmin(255,fmax(0,(int)img.at<Vec3b>(i+1,j+1)[k] + (1 * quant_error) / 16));
         }
      }
   }
}

// Spread the pixes according to a number of steps
void Interpreter::spreadPixels(Mat &inImage, Mat &outImage, int step, int numBits = 8){
   Colour cl;
   Mat img = inImage.clone();
   outImage = Mat::zeros(inImage.size(), inImage.type());
   //outImage = img.clone();
   uchar maskBit = 0xFF;// Make all the bits 1
   // keep numBits as 1 and (8 - numBits) would be all 0 towards the right
   maskBit = maskBit << (8 - numBits);
   for(int rowInd = 0; rowInd < img.rows; rowInd=rowInd+step ){
      for(int colInd = 0; colInd < img.cols; colInd=colInd+step ){
         Vec3b newpixel = cl.quantitizePixel(img.at<Vec3b>(rowInd,colInd), maskBit);
         outImage.at<Vec3b>(rowInd,colInd) = newpixel;
      }
   }
}
// Include a circle according to steps but only if its colour is different from the current background
int Interpreter::circulate(Mat &inImage, Mat &outImage, int step,
         SVG &svg, float threshold = 10.0){
   Colour cl;
   int circleCount = 0;
   //Mat1b mask(inImage.rows, inImage.cols, uchar(0));// Mask to calculate the avarage
   Mat inImageLAB;
   Mat outImageLAB;
   Circle inImageCircle, inImageLABCircle, outImageLABCircle;
   inImageCircle.setRadius(step/2);
   inImageLABCircle.setRadius(step/2);
   outImageLABCircle.setRadius(step/2);
   // Make a LAB copy of the image
   cl.convertImgToLAB(inImage, inImageLAB);
   cl.convertImgToLAB(outImage, outImageLAB);
   for(int rowInd = step/2; rowInd < inImage.rows; rowInd=rowInd+step ){
      for(int colInd = step/2; colInd < inImage.cols; colInd=colInd+step ){
         inImageCircle.setCenter(Point(colInd, rowInd));
         inImageLABCircle.setCenter(Point(colInd, rowInd));
         outImageLABCircle.setCenter(Point(colInd, rowInd));
         inImageLABCircle.setColour(inImageLAB, 5);
         outImageLABCircle.setColour(outImageLAB, 5);
         if(inImageLABCircle.compareColours(outImageLABCircle, threshold)){
            inImageCircle.setColour(inImage, 5);
            inImageCircle.draw(outImage);
            svg.add(inImageCircle);
            circleCount++;
         }
      }
   }

   
   
   //std::cout<<"Number of circles: "<<circleCount<<"\n";
   return circleCount;
}
// Make a img representation using circles
void Interpreter::representWithCircles(Mat &inImage, Mat &outImage, int numCircles,
                     std::string filename = "output", std::string path = "images/", 
                     float threshold = 10.0, float growFactor = 1.3, 
                     int maxAttempts = 20){
   SVG svg(inImage);
   int diameter = (inImage.cols > inImage.rows)? inImage.rows :inImage.cols;
   int minDiamete = 10;// Dont want circles with less than 10 pixels
   int circleCount = 0;
   int attempts = 0;
   int newCircles = 1;
   while(circleCount < numCircles && newCircles > 0 && attempts<= maxAttempts && diameter >= minDiamete){
      newCircles = circulate(inImage, outImage, diameter,svg, threshold);
      circleCount += newCircles;
      threshold *= growFactor;
      diameter /= 2;
      attempts++;
   }

   filename+="_Circles";
   svg.save(filename, path);

   std::cout<<"Total circles: "<<circleCount<<"\n";
}
// Spread polygons in a img according to a threshold
void Interpreter::representWithPolygon(Mat &inImage, Mat &outImage, PolyType pType,
                                    int numRect, float threshold = 10.0, 
                                    float growFactor = 1.3, int maxAttempts = 8){
   int rowStep = inImage.rows;
   int colStep = inImage.cols;
   int polyCount = 0;
   int attempts = 0;
   int newPoly = 1;
   int divisor = 1;
   while(polyCount < numRect && newPoly > 0 && attempts<= maxAttempts){
      newPoly = polygonate(inImage, outImage, pType, inImage.rows/divisor,
                              inImage.cols/divisor, threshold);
      polyCount += newPoly;
      threshold *= growFactor;
      divisor *= 2;
      attempts++;
   }
   std::cout<<"Total polygons: "<<polyCount<<"\n";
}

// Make a img representation using the polygon type
int Interpreter::polygonate(Mat &inImage, Mat &outImage, PolyType pType,
                           int rowStep, int colStep, 
                           float threshold = 10.0){
   Colour cl;
   int polyCount = 0;
   Mat inImageLAB;
   Mat outImageLAB;
   // Make a LAB copy of the image
   cl.convertImgToLAB(inImage, inImageLAB);
   cl.convertImgToLAB(outImage, outImageLAB);
   Polygon inImagePoly, inImageLABPoly, outImageLABPoly;
   for(int rowInd = 0; rowInd <= inImage.rows - rowStep; rowInd+=rowStep ){
      for(int colInd = 0; colInd <= inImage.cols - colStep; colInd+=colStep ){

         std::vector<Polygon> polyList = getPolygon(pType, colInd,
                                 colInd+colStep, rowInd+rowStep, rowInd);
         for(int pInd = 0; pInd < polyList.size(); pInd++){

            inImagePoly.setPoints(polyList[pInd]);
            inImageLABPoly.setPoints(polyList[pInd]);
            outImageLABPoly.setPoints(polyList[pInd]);

            inImageLABPoly.setColour(inImageLAB, 5);
            outImageLABPoly.setColour(outImageLAB, 5);
            
            if(inImageLABPoly.compareColours(outImageLABPoly, threshold)){
               inImagePoly.setColour(inImage, 5);
               inImagePoly.draw(outImage);
               polyCount++;
            }
         }
      }
   }
   return polyCount;
}

// Make a delaunay triangulation using the corners extracted by goodfeatures
void Interpreter::doTheDelaunay(Mat &inImage, Mat &outImage, 
         std::string filename = "output", std::string path = "images/",
         std::ofstream* dataOut = NULL){
   Colour cl;
   Points pt;
   Mesh ms;
   SVG svg(inImage);
   int polyCount = 0;
   // Make the LAB reference
   /*
   Mat labReduced;
   cl.quantizeLABImage(inImage, labReduced, 8, 8);
   */
   Mat labReduced;
   cl.toLAB(inImage, labReduced);
   // Get the corner points list
   std::vector<Point2f> points;
   pt.getPoints(Points::GOODFEATURE, labReduced, points);
   // Triangulate the points

   ms.triangulate(Mesh::DELAUNAY, points, inImage);
   ms.print(outImage);

   svg.add(ms.getPolygons());
   svg.save(filename, path);
   /*
   ms.testEdgeRotation(inImage);// Only a test
   ms.print(outImage);

   SVG svg2(inImage);
   svg2.add(ms.getPolygons());
   filename+="_Rotated";
   svg2.save(filename, path);
   */
   polyCount = ms.getNumPolys();
   if(dataOut){ *dataOut<< polyCount << "|";}
   //std::cout<<"Delaunay drawn with "<<polyCount<<" triangles. \n";
}

// Returns a mesh with the images's Delaunay triangulation
Mesh Interpreter::makeDelaunayMesh(Mat &inImage,
                                   std::ofstream* dataOut = NULL){
   Colour cl;
   Points pt;
   Mesh ms;
   Timer clk;

   if(dataOut){clk.start();}
   // Make the LAB reference
   Mat labReduced;
   cl.toLAB(inImage, labReduced);
   //Time to convert image to LAB colour space
   if(dataOut){ *dataOut<< clk.stop() << "|";}
   
   if(dataOut){clk.start();}
   // Get the corner points list
   std::vector<Point2f> points;
   pt.getPoints(Points::GOODFEATURE, labReduced, points);
   //Time to getPoints
   if(dataOut){ *dataOut<< clk.stop() << "|";}
   
   if(dataOut){clk.start();}
   // Triangulate the mesh
   ms.triangulate(Mesh::DELAUNAY, points, inImage);
   //Time to triangulate
   if(dataOut){ *dataOut<< clk.stop() << "|";}

   return ms;
}

// Add a list of edges to a mesh
Mesh Interpreter::addEdgesToMesh(Mat &inImage, std::vector<Vec4f> edges,
                                   std::ofstream* dataOut = NULL){
   Mesh ms;
   Timer clk;
   std::vector<Point2f> points;
   Rect rect(Point2f(), inImage.size());

   for (int i = 0; i < edges.size(); ++i)
   {
      Vec4f ed = edges[i];
      Point2f p1 = Point2f(ed[0], ed[1]);
      Point2f p2 = Point2f(ed[2], ed[3]);
      // CREAT A CHECK IF THE POINTS ARE IN THE IMG LIMITS
      if(ed[0] >= 0 && ed[1] >= 0 && ed[0] <= inImage.cols && ed[1] <= inImage.rows){
         points.push_back(p1);
         //std::cout<<"Point: "<<ed[0]<<" - "<<ed[1]<<" | Out of Bounderies\n";
      }
      if(ed[2] >= 0 && ed[3] >= 0 && ed[2] <= inImage.cols && ed[3] <= inImage.rows){
         points.push_back(p2);
         //std::cout<<"Point: "<<ed[2]<<" - "<<ed[3]<<" | Out of Bounderies\n";
      }
      /*
      if(rect.contains(p1)){
         points.push_back(p1);
      }
      if(rect.contains(p1)){
         points.push_back(p2);
      }
      */
   }
   
   // Triangulate the mesh
   ms.triangulate(Mesh::DELAUNAY, points, inImage);
   
   return ms;
}

void Interpreter::simulatedAnnealing(Mat &inImage, Mat &outImage, 
         std::string filename = "output", std::string path = "images/",
         std::ofstream* dataOut = NULL){
   Mat propose = Mat::zeros(inImage.size(), inImage.type());
   
   Timer clkPRI;
   Timer clkPSNR;
   Timer clkRotation;
   Timer clkOther;
   double rotationTime;
   double PSNRTime;
   double printTime;
   double otherTime;

   Mesh ms = makeDelaunayMesh(inImage, dataOut);
   Mesh msNew = ms;

   ms.print(propose);  
   
   double oldPSNR = getPSNR(inImage, propose);
   double originialPSNR =  oldPSNR;

   // This is the proper annealing simmulation
   const double C = 1.5;
   int k = 1;
   int attempts = 0;
   int maxAttempts = 1500;
   double newPSNR;
   double temp;
   double alpha;

   while(attempts<maxAttempts){// CHANGE IT  
      clkRotation.start();
      std::vector<int> rotations(1, -1);
      msNew.rotateEdges(rotations, inImage);
      rotationTime += clkRotation.stop();

      clkPRI.start();
      msNew.print(propose);
      printTime += clkPRI.stop();

      clkPSNR.start();
      newPSNR = getPSNR(inImage, propose);
      PSNRTime += clkPSNR.stop();

      clkOther.start();
      temp = C / log(1+k);
      alpha = pow((newPSNR/oldPSNR),(1/temp));
      if(random(1.0, alpha) > random(1.0,0.0)){
         ms = msNew;
         oldPSNR = newPSNR;
         k++;
      }else{
         msNew = ms;
      }
      attempts++;
      otherTime += clkOther.stop();
   }
   std::cout<<"Sim Ann - Initial PSNR: "<<originialPSNR<<"\n";
   std::cout<<"Sim Ann - Final PSNR: "<<newPSNR<<"\n";
   namedWindow("Final mesh", WINDOW_NORMAL );
   imshow("Final mesh", propose);

   // Rotation Time
   if(dataOut){ *dataOut<< rotationTime << "|";}
   if(dataOut){ *dataOut<< rotationTime/maxAttempts << "|";}
   // PSNR Time
   if(dataOut){ *dataOut<< PSNRTime << "|";}
   if(dataOut){ *dataOut<< PSNRTime/maxAttempts << "|";}
   // Print Time
   if(dataOut){ *dataOut<< printTime << "|";}
   if(dataOut){ *dataOut<< printTime/maxAttempts << "|";}
   // Other Time
   if(dataOut){ *dataOut<< otherTime << "|";}
   if(dataOut){ *dataOut<< otherTime/maxAttempts << "|";}
   // Original and new PSNR
   if(dataOut){ *dataOut<< originialPSNR << "|";}
   if(dataOut){ *dataOut<< newPSNR << "|";}
   /*
   svg.add(ms.getPolygons());
   filename+="_SimAnn";
   svg.save(filename, path);
   */
}

void Interpreter::greedySearch(Mat &inImage, Mat &outImage, 
         std::string filename = "output", std::string path = "images/",
         std::ofstream* dataOut = NULL){
   Mat propose = Mat::zeros(inImage.size(), inImage.type());
   Mesh ms = makeDelaunayMesh(inImage, dataOut);
   Mesh msNew = ms;

   ms.print(propose);  
   
   double oldPSNR = getPSNR(inImage, propose);
   double originialPSNR =  oldPSNR;
   double newPSNR;
   for(int trys = 0 ; trys < 1 ; trys++){
      for (int i = 0; i < ms.getNumEdges(); i++){
         if(msNew.rotateEdge(i, inImage)){
            msNew.print(propose);
            newPSNR = getPSNR(inImage, propose);
            if(newPSNR > oldPSNR){
               ms = msNew;
               oldPSNR = newPSNR;
            }else{
               msNew = ms;
            }
         }
      }
   }
   std::cout<<"Greedy - Initial PSNR: "<<originialPSNR<<"\n";
   std::cout<<"Greedy - Final PSNR: "<<newPSNR<<"\n";
   // Original and new PSNR
   if(dataOut){ *dataOut<< originialPSNR << "|";}
   if(dataOut){ *dataOut<< newPSNR << "|";}

   namedWindow("Final mesh", WINDOW_NORMAL );
   imshow("Final mesh", propose);
}

// returns a list of polygons according to the type and coordinates
std::vector<Polygon> Interpreter::getPolygon(PolyType pType,int 
                                 lowCol,int upCol,int lowRow,int upRow){
   switch(pType){
      case RECTANGLE:
         return getRectangle(lowCol,upCol,lowRow,upRow);
         break;

      case L_R:
         return getLRTriangle(lowCol,upCol,lowRow,upRow);
         break;

      case R_L:
         return getRLTriangle(lowCol,upCol,lowRow,upRow);
         break;

      case X:
         return getXTriangle(lowCol,upCol,lowRow,upRow);
         break;

      case DIAMOND:
         return getDiamond(lowCol,upCol,lowRow,upRow);
         break;

      case DIAMOND_H:
         return getDiamondH(lowCol,upCol,lowRow,upRow);
         break;

      case DIAMOND_V:
         return getDiamondV(lowCol,upCol,lowRow,upRow);
         break;

      case DIAMOND_X:
         return getDiamondX(lowCol,upCol,lowRow,upRow);
         break;

      case SUN:
         return getSUN(lowCol,upCol,lowRow,upRow);
         break;

      case RAND:
      default:
         //select a Random PolyType
         return getPolygon(static_cast<PolyType>(rand() % SUN),
                                    lowCol,upCol,lowRow,upRow);
         break;
   }
}

// Private Functions

std::vector<Polygon> Interpreter::getRectangle(int lowCol,int upCol,
                                               int lowRow,int upRow){
   std::vector<Polygon> list;
   Polygon t1;
   // MUST order in the way they apear on the polygon, or the center wont work
   t1.addPoint(Point(lowCol, upRow)); //topLeft
   t1.addPoint(Point(upCol, upRow)); //topRight
   t1.addPoint(Point(upCol, lowRow)); //bottomRight
   t1.addPoint(Point(lowCol, lowRow)); //bottomLeft

   list.push_back(t1);
   return list;
}

std::vector<Polygon> Interpreter::getLRTriangle(int lowCol,int upCol,
                                                int lowRow,int upRow){
   std::vector<Polygon> list;
   Polygon t1;
   // MUST order in the way they apear on the polygon, or the center wont work
   t1.addPoint(Point(lowCol, upRow)); //topLeft
   t1.addPoint(Point(upCol, upRow)); //topRight
   t1.addPoint(Point(upCol, lowRow)); //bottomRight

   list.push_back(t1);

   Polygon t2;
   // MUST order in the way they apear on the polygon, or the center wont work
   t2.addPoint(Point(lowCol, upRow)); //topLeft
   t2.addPoint(Point(lowCol, lowRow)); //bottomLeft
   t2.addPoint(Point(upCol, lowRow)); //bottomRight

   list.push_back(t2);
   return list;
}

std::vector<Polygon> Interpreter::getRLTriangle(int lowCol,int upCol,
                                                int lowRow,int upRow){
   std::vector<Polygon> list;
   Polygon t1;
   // MUST order in the way they apear on the polygon, or the center wont work
   t1.addPoint(Point(upCol, upRow)); //topRight
   t1.addPoint(Point(upCol, lowRow)); //bottomRight
   t1.addPoint(Point(lowCol, lowRow)); //bottomLeft

   list.push_back(t1);

   Polygon t2;
   // MUST order in the way they apear on the polygon, or the center wont work
   t2.addPoint(Point(lowCol, upRow)); //topLeft
   t2.addPoint(Point(upCol, upRow)); //topRight
   t2.addPoint(Point(lowCol, lowRow)); //bottomLeft
   list.push_back(t2);
   return list;
}

std::vector<Polygon> Interpreter::getXTriangle(int lowCol,int upCol,
                                                int lowRow,int upRow){
   Point center = Point((lowCol+upCol)/2, (lowRow+upRow)/2);
   Point topLeft = Point(lowCol, upRow);
   Point bottomLeft = Point(lowCol, lowRow);
   Point topRight = Point(upCol, upRow);
   Point bottomRight = Point(upCol, lowRow);
   std::vector<Polygon> list;

   Polygon t1;
   // MUST order in the way they apear on the polygon, or the center wont work
   t1.addPoint(topLeft); //topLeft
   t1.addPoint(center); //centerPoint
   t1.addPoint(bottomLeft); //bottomLeft
   list.push_back(t1);

   Polygon t2;
   // MUST order in the way they apear on the polygon, or the center wont work
   t2.addPoint(bottomLeft); //bottomLeft
   t2.addPoint(center); //centerPoint
   t2.addPoint(bottomRight); //bottomRight
   list.push_back(t2);

   Polygon t3;
   // MUST order in the way they apear on the polygon, or the center wont work
   t3.addPoint(bottomRight);
   t3.addPoint(center);
   t3.addPoint(topRight);
   list.push_back(t3);
   
   Polygon t4;
   // MUST order in the way they apear on the polygon, or the center wont work
   t4.addPoint(topRight);
   t4.addPoint(center);
   t4.addPoint(topLeft);
   list.push_back(t4);

   return list;
}

std::vector<Polygon> Interpreter::getDiamond(int lowCol,int upCol,
                                                int lowRow,int upRow){
   Point center = Point((lowCol+upCol)/2, (lowRow+upRow)/2);
   Point midTop = Point((lowCol+upCol)/2,upRow);
   Point midBottom = Point((lowCol+upCol)/2,lowRow);
   Point midLeft = Point(lowCol,(lowRow+upRow)/2);
   Point midRight = Point(upCol,(lowRow+upRow)/2);
   Point topLeft = Point(lowCol, upRow);
   Point bottomLeft = Point(lowCol, lowRow);
   Point topRight = Point(upCol, upRow);
   Point bottomRight = Point(upCol, lowRow);
   std::vector<Polygon> list;

   Polygon t1;
   // MUST order in the way they apear on the polygon, or the center wont work
   t1.addPoint(topLeft);
   t1.addPoint(midTop);
   t1.addPoint(midLeft);
   list.push_back(t1);

   Polygon t2;
   // MUST order in the way they apear on the polygon, or the center wont work
   t2.addPoint(bottomLeft); //bottomLeft
   t2.addPoint(midLeft); //centerPoint
   t2.addPoint(midBottom); //bottomRight
   list.push_back(t2);

   Polygon t3;
   // MUST order in the way they apear on the polygon, or the center wont work
   t3.addPoint(bottomRight);
   t3.addPoint(midRight);
   t3.addPoint(midBottom);
   list.push_back(t3);
   
   Polygon t4;
   // MUST order in the way they apear on the polygon, or the center wont work
   t4.addPoint(topRight);
   t4.addPoint(midTop);
   t4.addPoint(midRight);
   list.push_back(t4);

   Polygon t5;
   // MUST order in the way they apear on the polygon, or the center wont work
   t5.addPoint(midTop);
   t5.addPoint(midRight);
   t5.addPoint(midBottom);
   t5.addPoint(midLeft);
   list.push_back(t5);

   return list;
}

std::vector<Polygon> Interpreter::getDiamondH(int lowCol,int upCol,
                                                int lowRow,int upRow){
   Point center = Point((lowCol+upCol)/2, (lowRow+upRow)/2);
   Point midTop = Point((lowCol+upCol)/2,upRow);
   Point midBottom = Point((lowCol+upCol)/2,lowRow);
   Point midLeft = Point(lowCol,(lowRow+upRow)/2);
   Point midRight = Point(upCol,(lowRow+upRow)/2);
   Point topLeft = Point(lowCol, upRow);
   Point bottomLeft = Point(lowCol, lowRow);
   Point topRight = Point(upCol, upRow);
   Point bottomRight = Point(upCol, lowRow);
   std::vector<Polygon> list;

   Polygon t1;
   // MUST order in the way they apear on the polygon, or the center wont work
   t1.addPoint(topLeft);
   t1.addPoint(midTop);
   t1.addPoint(midLeft);
   list.push_back(t1);

   Polygon t2;
   // MUST order in the way they apear on the polygon, or the center wont work
   t2.addPoint(bottomLeft); //bottomLeft
   t2.addPoint(midLeft); //centerPoint
   t2.addPoint(midBottom); //bottomRight
   list.push_back(t2);

   Polygon t3;
   // MUST order in the way they apear on the polygon, or the center wont work
   t3.addPoint(bottomRight);
   t3.addPoint(midRight);
   t3.addPoint(midBottom);
   list.push_back(t3);
   
   Polygon t4;
   // MUST order in the way they apear on the polygon, or the center wont work
   t4.addPoint(topRight);
   t4.addPoint(midTop);
   t4.addPoint(midRight);
   list.push_back(t4);

   Polygon t5;
   // MUST order in the way they apear on the polygon, or the center wont work
   t5.addPoint(midTop);
   t5.addPoint(midRight);
   t5.addPoint(midLeft);
   list.push_back(t5);

   Polygon t6;
   // MUST order in the way they apear on the polygon, or the center wont work
   t6.addPoint(midRight);
   t6.addPoint(midBottom);
   t6.addPoint(midLeft);
   list.push_back(t6);

   return list;
}

std::vector<Polygon> Interpreter::getDiamondV(int lowCol,int upCol,
                                                int lowRow,int upRow){
   Point center = Point((lowCol+upCol)/2, (lowRow+upRow)/2);
   Point midTop = Point((lowCol+upCol)/2,upRow);
   Point midBottom = Point((lowCol+upCol)/2,lowRow);
   Point midLeft = Point(lowCol,(lowRow+upRow)/2);
   Point midRight = Point(upCol,(lowRow+upRow)/2);
   Point topLeft = Point(lowCol, upRow);
   Point bottomLeft = Point(lowCol, lowRow);
   Point topRight = Point(upCol, upRow);
   Point bottomRight = Point(upCol, lowRow);
   std::vector<Polygon> list;

   Polygon t1;
   // MUST order in the way they apear on the polygon, or the center wont work
   t1.addPoint(topLeft);
   t1.addPoint(midTop);
   t1.addPoint(midLeft);
   list.push_back(t1);

   Polygon t2;
   // MUST order in the way they apear on the polygon, or the center wont work
   t2.addPoint(bottomLeft); //bottomLeft
   t2.addPoint(midLeft); //centerPoint
   t2.addPoint(midBottom); //bottomRight
   list.push_back(t2);

   Polygon t3;
   // MUST order in the way they apear on the polygon, or the center wont work
   t3.addPoint(bottomRight);
   t3.addPoint(midRight);
   t3.addPoint(midBottom);
   list.push_back(t3);
   
   Polygon t4;
   // MUST order in the way they apear on the polygon, or the center wont work
   t4.addPoint(topRight);
   t4.addPoint(midTop);
   t4.addPoint(midRight);
   list.push_back(t4);

   Polygon t5;
   // MUST order in the way they apear on the polygon, or the center wont work
   t5.addPoint(midTop);
   t5.addPoint(midRight);
   t5.addPoint(midBottom);
   list.push_back(t5);

   Polygon t6;
   // MUST order in the way they apear on the polygon, or the center wont work
   t6.addPoint(midTop);
   t6.addPoint(midLeft);
   t6.addPoint(midBottom);
   list.push_back(t6);

   return list;
}

std::vector<Polygon> Interpreter::getDiamondX(int lowCol,int upCol,
                                                int lowRow,int upRow){
   Point center = Point((lowCol+upCol)/2, (lowRow+upRow)/2);
   Point midTop = Point((lowCol+upCol)/2,upRow);
   Point midBottom = Point((lowCol+upCol)/2,lowRow);
   Point midLeft = Point(lowCol,(lowRow+upRow)/2);
   Point midRight = Point(upCol,(lowRow+upRow)/2);
   Point topLeft = Point(lowCol, upRow);
   Point bottomLeft = Point(lowCol, lowRow);
   Point topRight = Point(upCol, upRow);
   Point bottomRight = Point(upCol, lowRow);
   std::vector<Polygon> list;

   Polygon t1;
   // MUST order in the way they apear on the polygon, or the center wont work
   t1.addPoint(topLeft);
   t1.addPoint(midTop);
   t1.addPoint(midLeft);
   list.push_back(t1);

   Polygon t2;
   // MUST order in the way they apear on the polygon, or the center wont work
   t2.addPoint(bottomLeft); //bottomLeft
   t2.addPoint(midLeft); //centerPoint
   t2.addPoint(midBottom); //bottomRight
   list.push_back(t2);

   Polygon t3;
   // MUST order in the way they apear on the polygon, or the center wont work
   t3.addPoint(bottomRight);
   t3.addPoint(midRight);
   t3.addPoint(midBottom);
   list.push_back(t3);
   
   Polygon t4;
   // MUST order in the way they apear on the polygon, or the center wont work
   t4.addPoint(topRight);
   t4.addPoint(midTop);
   t4.addPoint(midRight);
   list.push_back(t4);

   Polygon t5;
   // MUST order in the way they apear on the polygon, or the center wont work
   t5.addPoint(midTop);
   t5.addPoint(midRight);
   t5.addPoint(center);
   list.push_back(t5);

   Polygon t6;
   // MUST order in the way they apear on the polygon, or the center wont work
   t6.addPoint(midTop);
   t6.addPoint(midLeft);
   t6.addPoint(center);
   list.push_back(t6);

   Polygon t7;
   // MUST order in the way they apear on the polygon, or the center wont work
   t7.addPoint(midBottom);
   t7.addPoint(midRight);
   t7.addPoint(center);
   list.push_back(t7);

   Polygon t8;
   // MUST order in the way they apear on the polygon, or the center wont work
   t8.addPoint(midBottom);
   t8.addPoint(midLeft);
   t8.addPoint(center);
   list.push_back(t8);

   return list;
}

std::vector<Polygon> Interpreter::getSUN(int lowCol,int upCol,
                                                int lowRow,int upRow){
   Point center = Point((lowCol+upCol)/2, (lowRow+upRow)/2);
   Point midTop = Point((lowCol+upCol)/2,upRow);
   Point midBottom = Point((lowCol+upCol)/2,lowRow);
   Point midLeft = Point(lowCol,(lowRow+upRow)/2);
   Point midRight = Point(upCol,(lowRow+upRow)/2);
   Point topLeft = Point(lowCol, upRow);
   Point bottomLeft = Point(lowCol, lowRow);
   Point topRight = Point(upCol, upRow);
   Point bottomRight = Point(upCol, lowRow);
   std::vector<Polygon> list;

   Polygon t1;
   // MUST order in the way they apear on the polygon, or the center wont work
   t1.addPoint(topLeft);
   t1.addPoint(midTop);
   t1.addPoint(center);
   list.push_back(t1);

   Polygon t2;
   // MUST order in the way they apear on the polygon, or the center wont work
   t2.addPoint(topLeft);
   t2.addPoint(midLeft);
   t2.addPoint(center);
   list.push_back(t2);

   Polygon t3;
   // MUST order in the way they apear on the polygon, or the center wont work
   t3.addPoint(midLeft);
   t3.addPoint(center);
   t3.addPoint(bottomLeft);
   list.push_back(t3);
   
   Polygon t4;
   // MUST order in the way they apear on the polygon, or the center wont work
   t4.addPoint(bottomLeft);
   t4.addPoint(midBottom);
   t4.addPoint(center);
   list.push_back(t4);

   Polygon t5;
   // MUST order in the way they apear on the polygon, or the center wont work
   t5.addPoint(midBottom);
   t5.addPoint(center);
   t5.addPoint(bottomRight);
   list.push_back(t5);

   Polygon t6;
   // MUST order in the way they apear on the polygon, or the center wont work
   t6.addPoint(midRight);
   t6.addPoint(center);
   t6.addPoint(bottomRight);
   list.push_back(t6);

   Polygon t7;
   // MUST order in the way they apear on the polygon, or the center wont work
   t7.addPoint(midRight);
   t7.addPoint(center);
   t7.addPoint(topRight);
   list.push_back(t7);

   Polygon t8;
   // MUST order in the way they apear on the polygon, or the center wont work
   t8.addPoint(midTop);
   t8.addPoint(center);
   t8.addPoint(topRight);
   list.push_back(t8);

   return list;
}