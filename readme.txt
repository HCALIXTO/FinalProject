 - Compiling

   to update the makefile type on comand line:

      cmake .

   to make the binary, type on command line:

      make

- Start a new program

   1)copy the following files to the desired folder:

      1) abstractor.cpp
      2) CMakeLists.txt

   2)paste the files into the desired folder

   3)change the .cpp file's name

   4)update the file's name on the CMakeLists.txt

- Run the comparison with a JPEG compressed image

   ./abstractor ../Img/c.jpg ../Img/c_comp90.jpg
